#include <cfenv>
struct SetRoundingMode
{
    SetRoundingMode() {fesetround(FE_UPWARD);}
    ~SetRoundingMode() {}
};
SetRoundingMode declare_rounding_mode;
//#include "upward_rounding.h"
#include <iostream>
//
#include <ibex.h>
/*#include <CGAL/FPU.h>
CGAL::Protect_FPU_rounding<> P(CGAL_FE_UPWARD);
struct GetRoundingMode
{
    GetRoundingMode() {std::cout << "getroundingmode " << fegetround() <<'\n';}
    ~GetRoundingMode() {}
};

GetRoundingMode get_rounding_mode;*/


#include <CGAL/Simple_cartesian.h>

typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_2 Point_2;
typedef Kernel::Segment_2 Segment_2;

int main()
{
    //std::cout << "main " << CGAL::FPU_get_cw() << std::endl;
    // test pour ibex
    ibex::Function f("x","y","(x+y)^2");
	ibex::Function df(f,ibex::Function::DIFF);
	std::cout << "df=" << df << std::endl;

    // test pour CGAL
    Point_2 p(1,1), q(10,10);
    std::cout << "p = " << p << std::endl;
    std::cout << "q = " << q.x() << " " << q.y() << std::endl;
    std::cout << "sqdist(p,q) = "
                << CGAL::squared_distance(p,q) << std::endl;
    Segment_2 s(p,q);
    Point_2 m(5, 9);
    std::cout << "m = " << m << std::endl;
    std::cout << "sqdist(Segment_2(p,q), m) = "
                << CGAL::squared_distance(s,m) << std::endl;
    std::cout << "p, q, and m ";
    switch (CGAL::orientation(p,q,m)){
    case CGAL::COLLINEAR:
        std::cout << "are collinear\n";
        break;
    case CGAL::LEFT_TURN:
        std::cout << "make a left turn\n";
        break;
    case CGAL::RIGHT_TURN:
        std::cout << "make a right turn\n";
        break;
    }
    std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;//*/
    return 0;
}