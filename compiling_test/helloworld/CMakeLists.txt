cmake_minimum_required(VERSION 3.10)

# sets the project name
project(Test)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# add the executable
add_executable(helloworld test.cpp)