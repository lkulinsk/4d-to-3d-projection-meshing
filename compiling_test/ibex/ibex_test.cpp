#include <iostream>
#include <ibex.h>
using namespace std;
using namespace ibex;

int main()
{
    Function f("x","y","(x+y)^2");
	Function df(f,Function::DIFF);
	cout << "df=" << df << endl;
    return 0;
}