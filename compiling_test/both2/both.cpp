#include <cfenv>
#include <iostream>
#include <ibex.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/FPU.h>

typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_2 Point_2;
typedef Kernel::Segment_2 Segment_2;

int default_rounding_mode = fegetround();
int test = CGAL::FPU_get_cw();

int main()
{
    std::cout << "CGAL_FE_TONEAREST = " << CGAL_FE_TONEAREST << '\n';
    std::cout << "CGAL_FE_TOWARDZERO = " << CGAL_FE_TOWARDZERO << '\n';
    std::cout << "CGAL_FE_UPWARD = " << CGAL_FE_UPWARD << '\n';
    std::cout << "CGAL_FE_DOWNWARD = " << CGAL_FE_DOWNWARD << '\n';
    std::cout << "FE_TONEAREST = " << FE_TONEAREST << '\n';
    std::cout << "FE_TOWARDZERO = " << FE_TOWARDZERO << '\n';
    std::cout << "FE_UPWARD = " << FE_UPWARD << '\n';
    std::cout << "FE_DOWNWARD = " << FE_DOWNWARD << '\n';

    int ibex_rounding_mode = fegetround();
    std::cout << "Before main : Default rounding mode : " << default_rounding_mode << " " << test << '\n';
    std::cout << "At beginning of main : Current rounding mode : " << ibex_rounding_mode << " " << CGAL::FPU_get_cw() << '\n';
    //fesetround(default_rounding_mode);
    std::cout << "Correction at beggining of main : Current rounding mode : " << fegetround() << " " << CGAL::FPU_get_cw() << '\n';

    // test pour ibex
    {
        //CGAL::Protect_FPU_rounding<> hi(CGAL_FE_UPWARD);
        fesetround(FE_UPWARD);
        std::cout << "Ibex protected, Current rounding mode : " << fegetround()<< " " << CGAL::FPU_get_cw()  << '\n';
        //ibex::Function f("x","y","(x+y)^2");
        //ibex::Function df(f,ibex::Function::DIFF);
        //std::cout << "df=" << df << std::endl;
        fesetround(FE_TONEAREST);
    }

    std::cout << "Left Ibex, Current rounding mode : " << fegetround()<< " " << CGAL::FPU_get_cw()  << '\n';

    // test pour CGAL
    Point_2 p(1,1), q(10,10);
    Segment_2 s(p,q);
    Point_2 m(5, 9);

    return 0;
}