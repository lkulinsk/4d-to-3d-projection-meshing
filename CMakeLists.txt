cmake_minimum_required(VERSION 3.1...3.15)

project(DualContouring)

# Boost and its components
find_package(Boost QUIET REQUIRED)
if (NOT Boost_FOUND)
  message(STATUS "This project requires the Boost library, and will not be compiled.")
  return()  
endif()

# Ibex
find_package(PkgConfig QUIET)
pkg_check_modules(IBEX ibex REQUIRED QUIET)

# Eigen
find_package(Eigen3 REQUIRED NO_MODULE)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# For rounding purposes.
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -frounding-math")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic-errors -Wall -Wextra -Werror -Weffc++ -O3")
#Que faire de -Wsign-conversion ?

add_executable(dual_contouring src/dual_contouring.cpp)

# Adding Eigen flags
target_link_libraries (dual_contouring Eigen3::Eigen)

# Adding Ibex flags
target_link_libraries(dual_contouring ${IBEX_LIBRARIES})
target_include_directories(dual_contouring PUBLIC ${IBEX_INCLUDE_DIRS})
target_link_directories(dual_contouring PUBLIC ${IBEX_LIBRARY_DIRS})