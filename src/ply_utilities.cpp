#include <vector>
#include <array>
#include "types.h"
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <assert.h>

typedef std::vector<int> Face;
typedef std::vector<Face> Mesh;

void write_to_ply(const std::string& file_path, const Mesh& faces, const std::vector<Point_3>& points)
{
    std::ofstream file(file_path, std::ios::out | std::ios::trunc);
    if(!file.is_open())
        std::cout << "File couldn't be opened to write .ply\n";
    file << "ply\n";
    file << "format ascii 1.0\n";
    file << "element vertex " << points.size() << "\n";
    file << "property double x\nproperty double y\nproperty double z\n";
    file << "element face " << faces.size() << "\n";
    file << "property list uchar int vertex_index\n";
    file << "end_header\n";
    for(Point_3 pt : points)
        file << pt[0] << " " << pt[1] << " " << pt[2] << "\n";
    for(Face face : faces)
        {
        file << face.size() << " ";
        for(size_t i = 0; i < face.size() - 1; ++i)
            file << face[i] << " ";
        file << face[face.size() - 1] << "\n";
        }
    file.close();
}

void python_ply_normals(const std::string& file_path, const std::vector<Point_3>& points, const std::vector<Vector_3>& normals)
{
    assert(points.size() == normals.size());
    std::ofstream file(file_path, std::ios::out | std::ios::trunc);
    if(!file.is_open())
        std::cout << "File couldn't be opened to write .ply\n";
    file << "ply\n";
    file << "format ascii 1.0\n";
    file << "element vertex " << points.size() << "\n";
    file << "property double x\n";
    file << "property double y\n";
    file << "property double z\n";
    file << "property double nx\n";
    file << "property double ny\n";
    file << "property double nz\n";
    file << "end_header\n";
    for(size_t i = 0; i < points.size(); ++i)
        file << points[i][0] << " " << points[i][1] << " " << points[i][2] << " " << normals[i][0] << " " << normals[i][1] << " " << normals[i][2] << "\n";
    file.close();
}