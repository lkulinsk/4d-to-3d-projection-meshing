#ifndef TYPES_H
#define TYPES_H
#include <Eigen/Dense>
#include <boost/container_hash/hash.hpp>
#include <memory>
#include <ibex.h>
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

typedef Eigen::Vector3d Point_3;
typedef Eigen::Vector3d Vector_3;
typedef Eigen::Vector4d Point_4;
//typedef K::Vector_3 Vector_3;

struct VoxelData
{
    std::vector<Point_3> dual_points {};
    std::vector<std::vector<ibex::IntervalVector*>> connected_intersections {};
};

struct EdgeData
{
    std::vector<ibex::IntervalVector> intersections {};
};

struct VertexData
{
    Point_3 coord {};
};

typedef std::vector<std::vector<std::vector<VertexData>>> Grid_3;
typedef std::vector<std::vector<VertexData>> Grid_2;
typedef std::vector<VertexData> Grid_1;
typedef std::array<int, 3> Index_3;
typedef std::array<int, 4> Index_4;
typedef std::array<int, 3> Dimension;

enum EdgeDir {PX, PY, PZ};

typedef std::pair<Index_3, EdgeDir> EdgeIndex_3;

template<class T>
using Matrix_3 = std::vector<std::vector<std::vector<T>>>;
template<class T>
using Matrix_2 = std::vector<std::vector<T>>;
template<class T>
using Matrix_1 = std::vector<T>;

struct Index_3Hasher
{
    std::size_t operator()(const Index_3& idx) const
    {
        return boost::hash_range(idx.cbegin(), idx.cend());
    }
};

struct Index_4Hasher
{
    std::size_t operator()(const Index_4& idx) const
    {
        return boost::hash_range(idx.cbegin(), idx.cend());
    }
};

struct EdgeIndexHasher
{
    std::size_t operator()(const EdgeIndex_3& edge) const
    {
        size_t seed = edge.second;
        boost::hash_range(seed, edge.first.cbegin(), edge.first.cend());
        return seed;
    }
};

typedef std::unordered_map<Index_3, VoxelData, Index_3Hasher> VoxelsMap;
typedef std::unordered_map<EdgeIndex_3, EdgeData, EdgeIndexHasher> EdgesMap;

typedef std::vector<std::vector<std::vector<VertexData>>> VerticesGrid;

#endif