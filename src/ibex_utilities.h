#ifndef IBEX_UTILITIES_H
#define IBEX_UTILITIES_H
#include <ibex.h>
#include "types.h"

double evaluate(const ibex::Function& f, std::vector<double>& x);

Point_3 evaluate_vec(const ibex::Function& f, const std::vector<double>& x);

class CustomIbexCov;

#endif