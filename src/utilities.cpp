#include <vector>
#include <array>
#include <iostream>

//Temporary function to print a vector's content.
template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec)
{
    out << "Vector{";
    if(!vec.empty())
    {
        for(std::size_t i=0; i < vec.size() - 1; i++)
            out << vec[i] << ", ";
        out << vec[vec.size() - 1];
    }
    out << "}";
    return out;
}
template <typename T, size_t array_size>
std::ostream& operator<<(std::ostream& out, const std::array<T, array_size>& arr)
{
    out << "Array{";
    if(!arr.empty())
    {
        for(std::size_t i=0; i < arr.size() - 1; i++)
            out << arr[i] << ", ";
        out << arr[arr.size() - 1];
    } 
    out << "}";
    return out;
}

template<typename T>
T donothing(const T& elem)
{
    return elem;
}