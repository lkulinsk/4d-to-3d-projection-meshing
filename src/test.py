from locale import normalize
import numpy as np
import matplotlib.pyplot as plt

class ParseError(Exception):
    pass

def readstripline(f):
    return f.readline().rstrip("\n")

def parseline(f):
    return f.readline().rstrip("\n").split(" ")

def read_ply(file_path):
    with open(file_path, "r") as f:

        points = []
        normals = []

        f.readline()
        f.readline()
        elem_def = []
        while True:
            line = parseline(f)
            if line[0] == "end_header":
                break
            elif line[0] == "element":
                elem_def.append([line[1], int(line[2])])
            elif line[0] == "property":
                elem_def[-1].append([line[1], line[2]])
        for elem in elem_def:
            name, nb = elem[0], elem[1]
            for i in range(nb):
                line = [float(elem) for elem in parseline(f)]
                points.append(np.array(line[0:3]))
                normals.append(np.array(line[3:6]))
        return np.array(points), np.array(normals)

def plot_ply(file_path):
    points, normals = read_ply(file_path)

    fig = plt.figure(figsize=(7, 7))
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(points[:, 0], points[:, 1], points[:, 2])
    ax.quiver(points[:, 0], points[:, 1], points[:, 2], normals[:, 0], normals[:, 1], normals[:, 2], normalize=True)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

if __name__ == "__main__":
    plot_ply("data/py_maple2.ply")
    