#include "ibex_utilities.h"
//#include "types.h"
#include <ibex.h>
#include <vector>
#include <fstream>
#include <assert.h>
#include <memory>
#include <utility>

/* Evaluate an ibex function that returns a double, and that doesn't have an interval in its expression.
Outputs its result as a double.*/
double evaluate(const ibex::Function& f, std::vector<double>& x)
{
    ibex::IntervalVector x_interval(x.size());
    for (size_t i = 0; i < x.size(); i++)
    {
        x_interval[i] = x[i];
    }
    return f.eval(x_interval).mid();
}

/* Evaluate an ibex function that returns a 3D vector and that doesn't have an interval in its expression.
Outputs its result as a CGAL::Point_3. */
Point_3 evaluate_vec(const ibex::Function& f, const std::vector<double>& x)
{
    ibex::IntervalVector x_interval(x.size());
    for (size_t i = 0; i < x.size(); i++)
    {
        x_interval[i] = x[i];
    }
    ibex::Vector ans = f.eval_vector(x_interval).mid();
    return Point_3(ans[0], ans[1], ans[2]);
}
class CustomIbexCov
{
private:
    double time_before_init = 0;
    int nb_cells_before_init = 0;

public:
    std::unique_ptr<ibex::CovSolverData> pcov;

    CustomIbexCov() : pcov(nullptr) {} 

    void save(const char* filename) const
    {
        if(!pcov)
        {
            std::cout << "Can't save the file, pointer is null.\n";
            return;
        }
        pcov->save(filename);
    }

    void combine_cov(const ibex::CovSolverData& cov_to_add)
    {
        if(!pcov)
        {
            if(cov_to_add.size() == 0)
            {
                time_before_init += cov_to_add.time();
                nb_cells_before_init += cov_to_add.nb_cells();
            }
            else
            {
                cov_to_add.save("___tmpcov.cov");
                ibex::CovSolverData test("___tmpcov.cov");
                pcov = std::make_unique<ibex::CovSolverData>("___tmpcov.cov");
                pcov->set_time(pcov->time() + time_before_init);
                pcov->set_nb_cells(pcov->nb_cells() + nb_cells_before_init);
                if(remove("___tmpcov.cov") != 0)
                    std::cout << "Temporary ___tmpcov.cov couldn't be deleted" << '\n';
            }
            return;
        }
        
        assert(cov_to_add.size() == cov_to_add.nb_boundary() + cov_to_add.nb_inner() + cov_to_add.nb_pending() + cov_to_add.nb_solution() + cov_to_add.nb_unknown());
        assert(cov_to_add.nb_inner() == 0); // A implémenter un jour.

        // TODO unicity box, varset pour solution et boundary
        for(size_t i = 0; i < cov_to_add.nb_solution(); ++i)
            pcov->add_solution(cov_to_add.solution(i), cov_to_add.unicity(i), cov_to_add.solution_varset(i));
        for(size_t i = 0; i < cov_to_add.nb_inner(); ++i)
            pcov->add_inner(cov_to_add.inner(i));
        for(size_t i = 0; i < cov_to_add.nb_boundary(); ++i)
            pcov->add_boundary(cov_to_add.boundary(i), cov_to_add.boundary_varset(i));
        for(size_t i = 0; i < cov_to_add.nb_pending(); ++i)
            pcov->add_pending(cov_to_add.pending(i));
        for(size_t i = 0; i < cov_to_add.nb_unknown(); ++i)
            pcov->add_unknown(cov_to_add.unknown(i));

        pcov->set_nb_cells(pcov->nb_cells() + cov_to_add.nb_cells());
        pcov->set_time(pcov->time() + cov_to_add.time());
    }
};

void print_ibex_cov(const ibex::CovSolverData& cov)
{
    std::cout << "Printing ibex cov :\n";
    std::cout << "Temps écoulé : " << cov.time() << " sec\n";
    std::cout << "nb_boundary : " << cov.nb_boundary() <<  '\n';
    std::cout << "nb_cells : " << cov.nb_cells() <<'\n';
    std::cout << "nb_inner : " << cov.nb_inner() <<  '\n';
    std::cout << "nb_pending : " << cov.nb_pending() <<  '\n';
    std::cout << "nb_solution : " << cov.nb_solution() <<  '\n';
    std::cout << "nb_unknown : " << cov.nb_unknown() <<  '\n';
    std::cout <<  "The list contains " << cov.size() <<  " boxes." << "\n";
}

std::pair<std::vector<int>, int> boxes_connected_components(const std::vector<ibex::IntervalVector>& boxes)
{
    std::vector<std::vector<int>> graph(boxes.size(), std::vector<int>(0, 0));
    for(size_t i = 0; i < boxes.size(); ++i)
        for(size_t j = i+1; j < boxes.size(); ++j)
            if(boxes[i].intersects(boxes[j]))
            {
                graph[i].push_back(j);
                graph[j].push_back(i);
            }
    //std::cout << "Graphe : " << graph << "\n";

    //Calcul des composantes connexes du graphe.
    std::vector<int> cc(boxes.size(), -1);
    int cc_nb = 0;
    for(size_t i = 0; i < boxes.size(); ++i)
    {
        if(cc[i] == -1)
        {
            std::stack<int> to_visit;
            to_visit.push(i);
            cc[i] = cc_nb;
            while(!to_visit.empty())
            {
                int u = to_visit.top();
                to_visit.pop();
                for(int v : graph[u])
                    if(cc[v] == -1)
                    {
                        to_visit.push(v);
                        cc[v] = cc_nb;
                    }
            }
            cc_nb += 1;
        }
    }
    //std::cout << "Composantes connexes : " << cc << "\n";
    return {cc, cc_nb};
}