// This first include is essential to make CGAL and Ibex rounding modes compatible. However, the hack behind it may cause an issue one day.
#include "upward_rounding.h"
#include "dual_contouring.h"
#include "types.h"
#include "ibex_utilities.cpp"
#include <iomanip>
#include <ibex.h>
#include <algorithm>
#include <utility>
#include <memory>
#include <array>
#include <string>
#include <vector>
#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <chrono>
#include <fstream>

std::vector<Index_3> get_neighbours(const Index_3& idx, const Dimension& resolution)
{
    std::vector<Index_3> neighbours;
    
    if (idx[0] > 0)
            neighbours.push_back(Index_3 {idx[0] - 1, idx[1], idx[2]});
        if (idx[0] < resolution[0] - 1)
            neighbours.push_back(Index_3 {idx[0] + 1, idx[1], idx[2]});
        if (idx[1] > 0)
            neighbours.push_back(Index_3 {idx[0], idx[1] - 1, idx[2]});
        if (idx[1] < resolution[1] - 1)
            neighbours.push_back(Index_3 {idx[0], idx[1] + 1, idx[2]});
        if (idx[2] > 0)
            neighbours.push_back(Index_3 {idx[0], idx[1], idx[2] - 1});
        if (idx[2] < resolution[2] - 1)
            neighbours.push_back(Index_3 {idx[0], idx[1], idx[2] + 1});

        return neighbours;
}

/*std::vector<EdgeIndex_3> get_adjacent_edges(const Index_3& idx, const Dimension& resolution)
{
    std::vector<EdgeIndex_3> edges;
    std::vector<Index_3> neighbours = get_neighbours(idx, resolution);
    for(auto const vertex : neighbours)
        edges.push_back(EdgeIndex_3(idx, vertex));
    return edges;
}*/

template <class T>
Matrix_3<T> initialize(const Dimension& resolution, const T& init_value)
{
    return Matrix_3<T>(resolution[0], Matrix_2<T>(resolution[1], Matrix_1<T>(resolution[2], init_value)));
}




Point_3 index_to_coord(const Index_3& idx, const Point_3& corner_inf, const Point_3& corner_sup, const Dimension& resolution)
{
    std::array<double, 3> pt;
    for(std::size_t i = 0; i < 3; i++)
    {
        pt[i] = corner_inf[i] + static_cast<double>(idx[i]) / resolution[i] * (corner_sup[i] - corner_inf[i]);
    }
    return Point_3(pt[0], pt[1], pt[2]);
}

//Temporary function to print a vector's content.
template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec)
{
    out << "Vector{";
    for(std::size_t i=0; i < vec.size() - 1; i++)
        out << vec[i] << ", ";
    out << vec[vec.size() - 1] << "}";
    return out;
}


ibex::IntervalVector make_interval_from_segment(Point_3& a, Point_3& b, ibex::Interval& t_range)
{
    return {ibex::Interval(a[0], b[0]),
    ibex::Interval(a[1], b[1]),
    ibex::Interval(a[2], b[2]),
    t_range};
}

void test()
{
    ibex::System system("../data/maple.mbx");

    ibex::DefaultSolver solver(system, 1e-03);
    

    //std::cout << "system.args " << system.args << '\n';
    std::cout << "system.box " << system.box << '\n';
    //std::cout << "system.ctrs " << system.ctrs << '\n';
    std::cout << "system.f_ctrs " << system.f_ctrs << '\n';
    std::cout << "system.goal " << system.goal << '\n';
    std::cout << "system.id " << system.id << '\n';
    std::cout << "system.nb_ctr " << system.nb_ctr << '\n';
    std::cout << "system.nb_var " << system.nb_var << '\n';
    std::cout << "system.ops " << *system.ops << '\n';
    //std::cout << "system.var_names() " << print_vec(std::cout, std::vector<int> {1, 2, 3}) << '\n';


	/* Run the solver */
	//solver.solve(system.box);

	/* Display the solutions */
	//std::cout << solver.get_data() << std::endl;

	/* Report performances */
	//std::cout << "cpu time used=" << solver.get_time() << "s."<< std::endl;
	//std::cout << "number of cells=" << solver.get_nb_cells() << std::endl;

}


int main()
{
    //ibex::Function f("x", "y", "z", "sqrt(x^2 + y^2 + z^2)");
    //ibex::Function df(f, ibex::Function::DIFF);

    //std::cout << "f = " << f << '\n';
    //std::cout << "df = " << df << '\n';

    Dimension resolution = {99, 99, 99};
    Point_3 corner_inf = {-1, -1, -1};
    Point_3 corner_sup = {1, 1, 1};

    VoxelsMap voxels_map {};
    EdgesMap edges_map {};
    Dimension nb_vertices = {resolution[0] + 1, resolution[1] + 1, resolution[2] + 1};
    Matrix_3<VertexData> vertices_grid {initialize<VertexData>(nb_vertices, VertexData())};

    // 1. Evaluer la fonction en chaque point de la grille.
    for(int i = 0; i < nb_vertices[0]; i++)
        for(int j = 0; j < nb_vertices[1]; j++)
            for(int k = 0; k < nb_vertices[2]; k++)
                vertices_grid[i][j][k].coord = index_to_coord(Index_3 {i, j, k}, corner_inf, corner_sup, resolution);

    // 2. Création du système ibex
    
    /*auto start = std::chrono::high_resolution_clock::now();
    ibex::System system("../data/maple.mbx");
    ibex::DefaultSolver solver(system, 1e-3); //Rajouter eps_x_max plus tard.
    ibex::Interval t_range(-1, 1);
    CustomIbexCov final_cov;
    
    int nb_boundary = 0;
    int nb_cells = 0;
    int nb_inner = 0;
    int nb_pending = 0;
    int nb_solution = 0;
    int nb_unknown = 0;
    int size = 0;
    double ttime = 0;
    // 3. Calcul les boîtes ibex pour chaque segment maximal de la grille.
    for(int i = 0; i < nb_vertices[0]; i++)
    {
        //std::cout << i << '\n';
        for(int j = 0; j < nb_vertices[1]; j++)
        {
            solver.solve(make_interval_from_segment(vertices_grid[i][j][0].coord, vertices_grid[i][j][nb_vertices[2] - 1].coord, t_range));
            const ibex::CovSolverData& cov = solver.get_data();
            final_cov.combine_cov(cov);
            nb_boundary += cov.nb_boundary();
            nb_cells += cov.nb_cells();
            nb_inner += cov.nb_inner();
            nb_pending += cov.nb_pending();
            nb_solution += cov.nb_solution();
            nb_unknown += cov.nb_unknown();
            size += cov.size();
            ttime += cov.time();
            //std::cout << "i, j, nb_cells = " << i << ", " << j << ", " << cov.size() << '\n'; 
        }
    }
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
    std::cout << "\nTest Grille Dans Code :\nTemps écoulé : " << duration.count() << " " << final_cov.pcov->time() << " " << ttime << " sec\n";
    std::cout << "nb_boundary " << nb_boundary << " " << final_cov.pcov->nb_boundary() <<  '\n';
    std::cout << "nb_cells " << nb_cells << " " << final_cov.pcov->nb_cells() <<'\n';
    std::cout << "nb_inner " << nb_inner << " " << final_cov.pcov->nb_inner() <<  '\n';
    std::cout << "nb_pending " << nb_pending << " " << final_cov.pcov->nb_pending() <<  '\n';
    std::cout << "nb_solution " << nb_solution << " " << final_cov.pcov->nb_solution() <<  '\n';
    std::cout << "nb_unknown " << nb_unknown << " " << final_cov.pcov->nb_unknown() <<  '\n';
    std::cout <<  "The list contains " << size << " " << final_cov.pcov->size() <<  " boxes" << "\n";
    final_cov.save("../data/maple1.cov");

    auto start2 = std::chrono::high_resolution_clock::now();

    ibex::System system2("../data/maple_xy.mbx");
    ibex::DefaultSolver solver2(system2, 1e-9, 1e-6);
    solver2.solve(system2.box);
    const ibex::CovSolverData& cov = solver2.get_data();

    auto stop2 = std::chrono::high_resolution_clock::now();
    auto duration2 = std::chrono::duration_cast<std::chrono::seconds>(stop2 - start2);
    std::cout << "\nTest Grille Dans Ibex :\nTemps écoulé : " << duration2.count() << " " << cov.time() << " sec\n";
    std::cout << "cov.nb_boundary() " << cov.nb_boundary() << '\n';
    std::cout << "cov.nb_cells() " << cov.nb_cells() << '\n';
    std::cout << "cov.nb_inner() " << cov.nb_inner() << '\n';
    std::cout << "cov.nb_pending() " << cov.nb_pending() << '\n';
    std::cout << "cov.nb_solution() " << cov.nb_solution() << '\n';
    std::cout << "cov.nb_unknown() " << cov.nb_unknown() << '\n';
    std::cout <<  "The list contains " << cov.size() << " boxes" << '\n';
    cov.save("../data/maple_xy.cov");//*/

    auto start3 = std::chrono::high_resolution_clock::now();
    ibex::Function fg("../data/maple_fg.mbx");
    ibex::Variable a("a");
    ibex::Variable b("b");
    ibex::Interval z_range3(-1, 1);
    ibex::Interval t_range3(-1, 1);
    ibex::IntervalVector init_box3 {z_range3, t_range3};
    //CustomIbexCov final_cov;
    
    int nb_boundary3 = 0;
    int nb_cells3 = 0;
    int nb_inner3 = 0;
    int nb_pending3 = 0;
    int nb_solution3 = 0;
    int nb_unknown3 = 0;
    int size3 = 0;
    double ttime3 = 0;
    int64_t factory_time = 0;
    int64_t substitution_time = 0;
    int64_t solve_time = 0;
    int64_t readdata_time = 0;
    // 3. Calcul les boîtes ibex pour chaque segment maximal de la grille.
    for(int i = 0; i < nb_vertices[0]; i++)
    {
        //std::cout << i << '\n';
        for(int j = 0; j < nb_vertices[1]; j++)
        {
            auto _start = std::chrono::high_resolution_clock::now();
            const ibex::ExprConstant& _x = ibex::ExprConstant::new_scalar(vertices_grid[i][j][0].coord[0]);
            const ibex::ExprConstant& _y = ibex::ExprConstant::new_scalar(vertices_grid[i][j][0].coord[1]);
            ibex::Function h(a, b, fg(_x, _y, a, b), "h");
            auto _stop = std::chrono::high_resolution_clock::now();
            substitution_time += std::chrono::duration_cast<std::chrono::milliseconds>(_stop - _start).count();

            _start = std::chrono::high_resolution_clock::now();
            ibex::SystemFactory factory3;
            factory3.add_var(a);
            factory3.add_var(b);
            factory3.add_ctr(h(a, b)=0);
            ibex::System system3(factory3);
            _stop = std::chrono::high_resolution_clock::now();
            factory_time += std::chrono::duration_cast<std::chrono::milliseconds>(_stop - _start).count();
            
            _start = std::chrono::high_resolution_clock::now();
            ibex::DefaultSolver solver3(system3, 1e-3);
            solver3.solve(init_box3);
            _stop = std::chrono::high_resolution_clock::now();
            solve_time += std::chrono::duration_cast<std::chrono::milliseconds>(_stop - _start).count();

            _start = std::chrono::high_resolution_clock::now();
            const ibex::CovSolverData& cov = solver3.get_data();
            //final_cov.combine_cov(cov);
            nb_boundary3 += cov.nb_boundary();
            nb_cells3 += cov.nb_cells();
            nb_inner3 += cov.nb_inner();
            nb_pending3 += cov.nb_pending();
            nb_solution3 += cov.nb_solution();
            nb_unknown3 += cov.nb_unknown();
            size3 += cov.size();
            ttime3 += cov.time();
            _stop = std::chrono::high_resolution_clock::now();
            readdata_time += std::chrono::duration_cast<std::chrono::milliseconds>(_stop - _start).count();
            //std::cout << "i, j, nb_cells = " << i << ", " << j << ", " << cov.size() << '\n'; 
        }
    }
    auto stop3 = std::chrono::high_resolution_clock::now();
    auto duration3 = std::chrono::duration_cast<std::chrono::milliseconds>(stop3 - start3);
    std::cout << "\nTest 3 Temps total écoulé : " << duration3.count() << " ms\n";
    std::cout << "Temps substitution : " << substitution_time << " ms\n";
    std::cout << "Temps création factory : " << factory_time << " ms\n";
    std::cout << "Temps de résolution ibex (donné par ibex) : " << ttime3*1000 << " ms\n";
    std::cout << "Temps de résolution ibex (mesuré par nous) : " << solve_time << " ms\n";
    std::cout << "Data processing : " << readdata_time << " ms\n";
    std::cout << "nb_boundary " << nb_boundary3 <<  '\n';
    std::cout << "nb_cells " << nb_cells3 <<'\n';
    std::cout << "nb_inner " << nb_inner3 << '\n';
    std::cout << "nb_pending " << nb_pending3 << '\n';
    std::cout << "nb_solution " << nb_solution3 << '\n';
    std::cout << "nb_unknown " << nb_unknown3 << '\n';
    std::cout <<  "The list contains " << size3 <<  " boxes" << "\n";
    //final_cov.save("../data/maple1.cov");

    /*for(int i = 0; i < nb_vertices[0]; i++)
        for(int k = 0; k < nb_vertices[2]; k++)
        {
            solver.solve(make_interval_from_segment(vertices_grid[i][0][k].coord, vertices_grid[i][nb_vertices[1] - 1][k].coord, t_range));
            //std::cout << "i, j = " << i << ", " << j << "\nNb cells = " << solver.get_nb_cells() << '\n';
        }
    for(int j = 0; j < nb_vertices[1]; j++)
        for(int k = 0; k < nb_vertices[2]; k++)
        {
            solver.solve(make_interval_from_segment(vertices_grid[0][j][k].coord, vertices_grid[nb_vertices[0] - 1][j][k].coord, t_range));
            //std::cout << "i, j = " << i << ", " << j << "\nNb cells = " << solver.get_nb_cells() << '\n';
        }*/

    
    /*// 2. Evaluer en chaque arête si intersecte
    for(edge in grid.enumerate_edges())
    {
        if(grid.vertices[edge.u] * grid.vertices[edge.v]) <= 0)
        {
            interpol = ???
            edges_dict[edge] = EdgeData(interpol, evaluate(df, intrerpol))
            // Inversion arête ?
        }
    }

    //3. Evaluer chaque face.
    for(face in range grid.faces)
    {
        if...
        {
            faces_dict[face] = ...
        }
    }

    //4. Créer la quadrangulation
    mesh = ???
    for(edge in edge.dict)
    {
        for faces in edges.neighbours()
        {
            mesh.add(...)
        }
    }

    //5. Convertir en .obj ou .ply
    */

    return 0;
}
