#include "types.h"
#include <iostream>
#include <fstream>
#include <string>

//ATTENTION, il faudrait faire un parser propre.
//Par exemple, il faut qu'il y ait des espaces partout pour spécifier resolution.
//Ou encore, dans le nom des chemins, il ne peut pas y avoir d'espace.
void parse_params(
    const std::string& file_path,
    std::string& input_mbx_file,
    std::string& input_cov_file,
    std::string& output_cov_file,
    std::string& output_ply_file,
    Dimension& resolution,
    double& ibex_eps_max,
    double& ibex_eps_min,
    double& ball_system_threshold,
    bool& grid_in_mesh
    )
{
    std::cout << "\nparam_parser.cpp is shaky. Use with care.\n";

    std::fstream file(file_path , std::ios::in);
    if(!file.is_open())
    {
        std::cout << "Couldn't open " << file_path << "\nAborting.\n";
        return;
    }

    std::string tmp, tmp2;
    while(file >> tmp)
    {
        if(tmp == "input_mbx_file")
        {
            file >> tmp2 >> tmp;
            input_mbx_file = tmp.substr(1, tmp.size()-2);
        }
        else if(tmp == "input_cov_file")
        {
            file >> tmp2 >> tmp;
            input_cov_file = tmp.substr(1, tmp.size()-2);
        }
        else if(tmp == "output_cov_file")
        {
            file >> tmp2 >> tmp;
            output_cov_file = tmp.substr(1, tmp.size()-2);
        }
        else if(tmp == "output_ply_file")
        {
            file >> tmp2 >> tmp;
            output_ply_file = tmp.substr(1, tmp.size()-2);
        }
        else if(tmp == "resolution")
        {
            int i1, i2, i3;
            file >> tmp2 >> tmp2 >> i1 >> tmp2 >> i2 >> tmp2 >> i3 >> tmp2;
            resolution = {i1, i2, i3};
        }
        else if(tmp == "ibex_eps_max")
        {
            double d;
            file >> tmp2 >> d;
            ibex_eps_max = d;
        }
        else if(tmp == "ibex_eps_min")
        {
            double d;
            file >> tmp2 >> d;
            ibex_eps_min = d;
        }
        else if(tmp == "ball_system_threshold")
        {
            double d;
            file >> tmp2 >> d;
            ball_system_threshold = d;
        }
        else if(tmp == "mesh_in_grid")
        {
            bool b;
            file >> tmp2 >> std::boolalpha >> b;
            grid_in_mesh = b;
        }
    }
    file.close();
    //std::cout << input_mbx_file << " " << input_cov_file << " " << output_cov_file << " " << output_ply_file << " " << resolution << " " << ibex_eps_max << " " << ibex_eps_min << "\n";
}