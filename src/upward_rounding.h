#ifndef UPWARD_ROUNDING_H
#define UPWARD_ROUNDING_H
/*
This is a hack to have Ibex and CGAL work together.
Indeed, if both are included in a file in debug mode, IBEX will change the FPU rounding mode at the very start of the main function. However, CGAL keeps track of what rounding mode is used before the function main, to adapt its behaviour.
In order to make those two behaviours compatible, we manually update the rounding mode before the main function using this hack, and set it to what ibex sets it to be.

However, as this is not documented anywhere, our explanation may be wrong.
Surprising issues may arise from that.
*/

#include <cfenv>

struct SetRoundingMode
{
    SetRoundingMode() {fesetround(FE_UPWARD);}
    ~SetRoundingMode() {}
};
SetRoundingMode declare_rounding_mode;

#endif