import struct
import numpy as np

def read_uint32(f):
    return struct.unpack("I", f.read(4))[0]

def read_float64(f):
    return struct.unpack("d", f.read(8))[0]

def read_char(f):
    return str(f.read(1), 'UTF-8')

def read_ibex_cov_file(file_path):
    with open(file_path, "rb") as f:
        
        to_return = {}

        # Cov
        signature = str(f.read(19), 'UTF-8')
        f.read(1) # NULL character we drop
        format_level= read_uint32(f) # format_level
        format_identifying_sequence = [read_uint32(f) for _ in range(format_level+ 1)]
        format_version_sequence = [read_uint32(f) for _ in range(format_level+ 1)]
        box_dim = read_uint32(f) # dimension of the boxes (# of variables)
        to_return["signature"] = signature
        to_return["format_level"] = format_level
        to_return["format_identifying_sequence"] = format_identifying_sequence
        to_return["format_version_sequence"] = format_version_sequence
        to_return["box_dim"] = box_dim

        # CovList
        if format_level>= 1:
            N = read_uint32(f) # total number of boxes
            boxes = []
            for _ in range(N):
                lb, ub = [], []
                for _ in range(box_dim):
                    lb.append(read_float64(f))
                    ub.append(read_float64(f))
                boxes.append((np.array(lb), np.array(ub)))
            to_return["N"] = N
            to_return["boxes"] = boxes
        
        # CovIUList
        if format_level>= 2:
            Ni = read_uint32(f) # number of inner boxes
            inner_boxes = [read_uint32(f) for _ in range(Ni)]
            to_return["Ni"] = Ni
            to_return["inner_boxes"] = inner_boxes
        
        # CovIBUList
        if format_level>= 3:
            boudary_box_type_1 = read_uint32(f)
            Nb = read_uint32(f) # number of boundary boxes
            boundary_boxes = [read_uint32(f) for _ in range(Nb)]
            to_return["boudary_box_type_1"] = boudary_box_type_1
            to_return["Nb"] = Nb
            to_return["boundary_boxes"] = boundary_boxes

        # CovManifold
        if format_level>= 4:
            nb_eq = read_uint32(f)
            nb_ineq = read_uint32(f)
            boudary_box_type_1 = read_uint32(f)
            if nb_eq > 0:
                Ns = read_uint32(f) # number of solution boxes
                solution_boxes = []
                for _ in range(Ns):
                    solution_idx = read_uint32(f)
                    parametrization_variables = [read_uint32(f) for _ in range(box_dim - nb_eq)]
                    lb, ub = [], []
                    for _ in range(box_dim):
                        lb.append(read_float64(f))
                        ub.append(read_float64(f))
                    solution_boxes.append((solution_idx, parametrization_variables, np.array(lb), np.array(ub)))
                to_return["Ns"] = Ns
                to_return["solution_boxes"] = solution_boxes
            Nbb = read_uint32(f) # number of boundary(???) boxes
            Nbb_boxes = [] # TODO Change name later when I figured out what this does.
            for _ in range(Nbb):
                boundary_idx = read_uint32(f)
                parametrization_variables = [read_uint32(f) for _ in range(box_dim - nb_eq)]
                Nbb_boxes.append((boundary_idx, parametrization_variables))
            to_return["nb_eq"] = nb_eq
            to_return["nb_ineq"] = nb_ineq
            to_return["boudary_box_type_1"] = boudary_box_type_1
            to_return["Nbb"] = Nbb
            to_return["Nbb_boxes"] = Nbb_boxes
            
        # CovSolverData
        if format_level >= 5:
            variable_names = []
            for _ in range(box_dim):
                name = ""
                last_char = ""
                while last_char != '\x00':
                    name += last_char
                    last_char = read_char(f)
                variable_names.append(name)
            search_status = read_uint32(f)
            time_spent = read_float64(f)
            nb_cells = read_uint32(f)
            Np = read_uint32(f) # number of pending boxes
            pending_boxes = [read_uint32(f) for _ in range(Np)]
            to_return["variable_names"] = variable_names
            to_return["search_status"] = search_status
            to_return["time_spent"] = time_spent
            to_return["nb_cells"] = nb_cells
            to_return["Np"] = Np
            to_return["pending_boxes"] = pending_boxes
        
        return to_return

def print_data(data):
    for key, value in data.items():
        print(f"{key} : {value}")

if __name__ == "__main__":
    pass