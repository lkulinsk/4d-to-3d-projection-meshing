#ifndef DUAL_CONTOURING_H
#define DUAL_CONTOURING_H
#include "types.h"
#include <vector>

std::vector<Index_3> get_neighbours(const Index_3& idx, const Dimension& resolution);

std::vector<EdgeIndex_3> get_adjacent_edges(const Index_3& idx, const Dimension& resolution);

#endif