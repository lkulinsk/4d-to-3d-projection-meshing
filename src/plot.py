
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import ibex_parser
import sys

def plot(path_list, draw_boxes=False):
    fig = plt.figure(figsize=(7, 7))
    ax = fig.add_subplot(111, projection='3d')

    solution = []
    boundary = []
    inner = []
    pending = []
    unknown = []

    for path in path_list:
        data = ibex_parser.read_ibex_cov_file(path)

        solution_idx = [elem[0] for elem in data["solution_boxes"]]
        boundary_idx = [elem[0] for elem in data["Nbb_boxes"]]

        for i, box in enumerate(data["boxes"]):
            if i in solution_idx:
                solution.append(box)
            elif i in boundary_idx:
                boundary.append(box)
            elif i in data["pending_boxes"]:
                pending.append(box)
            elif i in data["inner_boxes"]:
                inner.append(box)
            else:
                unknown.append(box)

    all_points = []
    if not draw_boxes:
        for box_type, color in [(solution, "green"), (boundary, "blue"), (inner, "purple"), (pending, "grey"), (unknown, "red")]:
            points = []
            for box in box_type:
                point = (box[0] + box[1]) / 2
                points.append(point[0:3])
            all_points.extend(points)    
            np_points = np.array(points)
            if len(np_points.shape) != 1: #checks if not empty
                ax.scatter(np_points[:,0], np_points[:,1], np_points[:,2], color=color)
    else:
        for box_type, color in [(solution, "green"), (boundary, "blue"), (inner, "purple"), (pending, "grey"), (unknown, "red")]:
            for box in box_type:
                vertices = np.array([
                    [box[0][0], box[0][1], box[0][2]],
                    [box[1][0], box[0][1], box[0][2]],
                    [box[1][0], box[1][1], box[0][2]],
                    [box[0][0], box[1][1], box[0][2]],
                    [box[0][0], box[0][1], box[1][2]],
                    [box[1][0], box[0][1], box[1][2]],
                    [box[1][0], box[1][1], box[1][2]],
                    [box[0][0], box[1][1], box[1][2]]
                ])
                all_points.extend(vertices)
                faces = [
                    [vertices[0],vertices[1],vertices[2],vertices[3]],
                    [vertices[4],vertices[5],vertices[6],vertices[7]],
                    [vertices[0],vertices[1],vertices[5],vertices[4]],
                    [vertices[2],vertices[3],vertices[7],vertices[6]],
                    [vertices[1],vertices[2],vertices[6],vertices[5]],
                    [vertices[4],vertices[7],vertices[3],vertices[0]]
                ]
                ax.scatter(vertices[:, 0], vertices[:, 1], vertices[:, 2], alpha=0, color="black")
                ax.add_collection3d(Poly3DCollection(faces, facecolors=color, linewidths=1, edgecolors=color, alpha=1))

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # Axis limits
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)
    ax.set_xlim(min(all_points, key=lambda x : x[0])[0], max(all_points, key=lambda x : x[0])[0])
    ax.set_ylim(min(all_points, key=lambda x : x[1])[1], max(all_points, key=lambda x : x[1])[1])
    ax.set_zlim(min(all_points, key=lambda x : x[2])[2], max(all_points, key=lambda x : x[2])[2])
    ax.legend()
    plt.show()

if __name__ == "__main__":
    if len(sys.argv) == 1:
        path_list = ["data/cov/test.cov"]
    else:
        path_list = sys.argv[1:]
    plot(path_list)