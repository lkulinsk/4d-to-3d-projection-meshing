---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.0
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib import cm
%config Completer.use_jedi = False
%matplotlib notebook
```

```python
def box_collection(cinf, csup):
    vertices = np.array([
        [cinf[0], cinf[1], cinf[2]],
        [csup[0], cinf[1], cinf[2]],
        [csup[0], csup[1], cinf[2]],
        [cinf[0], csup[1], cinf[2]],
        [cinf[0], cinf[1], csup[2]],
        [csup[0], cinf[1], csup[2]],
        [csup[0], csup[1], csup[2]],
        [cinf[0], csup[1], csup[2]]
    ])
    faces = [
        [vertices[0],vertices[1],vertices[2],vertices[3]],
        [vertices[4],vertices[5],vertices[6],vertices[7]],
        [vertices[0],vertices[1],vertices[5],vertices[4]],
        [vertices[2],vertices[3],vertices[7],vertices[6]],
        [vertices[1],vertices[2],vertices[6],vertices[5]],
        [vertices[4],vertices[7],vertices[3],vertices[0]]
    ]
    return Poly3DCollection(faces)

def add_box(ax, cinf, csup, color):
    vertices = np.array([
                    [cinf[0], cinf[1], cinf[2]],
                    [csup[0], cinf[1], cinf[2]],
                    [csup[0], csup[1], cinf[2]],
                    [cinf[0], csup[1], cinf[2]],
                    [cinf[0], cinf[1], csup[2]],
                    [csup[0], cinf[1], csup[2]],
                    [csup[0], csup[1], csup[2]],
                    [cinf[0], csup[1], csup[2]]
                ])
    edges = [[0, 1], [1, 2], [2, 3], [0, 3], [4, 5], [5, 6], [6, 7], [4, 7], [0, 4], [1, 5], [2, 6], [3, 7]]
    #ax.scatter(vertices[:, 0], vertices[:, 1], vertices[:, 2], alpha=0, color="black")
    #ax.add_collection3d(Poly3DCollection(faces, facecolors=color, linewidths=0.3, edgecolors=color, alpha=0, zsort="max"))
    for edge in edges:
        x = [vertices[i][0] for i in edge]
        y = [vertices[i][1] for i in edge]
        z = [vertices[i][2] for i in edge]
        ax.plot(x, y, z, color="black", linewidth=0.5)
    
def configure_ax(ax):
    ax.set_axis_off()
    ax.grid(False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    ax.set_box_aspect([1, 1, 1])
```

```python
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111, projection='3d')
u, v = np.meshgrid(np.linspace(-1, 1, 100), np.linspace(-1, 1, 100))
x = u*v
y = u
z = v**2
#ax.plot_wireframe(x, y, z, color="r")
print(z.shape)
a1, a2 = np.meshgrid(np.linspace(0, 0.99, z.shape[0]), np.linspace(0, 0.99, z.shape[1]))
my_col = cm.viridis(a2)
surf = ax.plot_surface(x, y, z, facecolors = my_col, linewidth=1, antialiased=True, rstride=1, cstride=1)
ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_zlim([0, 2])
#fig.colorbar(surf, shrink=0.5, ticks=None)
#poly = box_collection([0, 0, 0], [1, 1, 1])
#poly.set_alpha(0)
configure_ax(ax)
ax.view_init(40, 60)
plt.show()
```

```python
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111, projection='3d')
"""patches = [np.meshgrid(np.linspace(1, 2, 50), np.linspace(1, 2, 50)),
          np.meshgrid(np.linspace(0, 1, 50), np.linspace(1, 2, 50)),
          np.meshgrid(np.linspace(1, 2, 50), np.linspace(0, 1, 50)),
          np.meshgrid(np.linspace(0, 1, 50), np.linspace(0, 1, 50))]
for patch in patches:
    X, Y = patch
    Z = 0.5 + np.sin(2 + X*0.5)/4
    surf = ax.plot_surface(X, Y, Z, color="blue")"""
def f(x, y):
    return (np.sin(x*np.pi*0.7) + np.sin(y*np.pi*0.7))/4.2 + 0.5
def dfx(x, y):
    return np.array([1, 0, np.pi*0.7*np.cos(x*np.pi*0.7)/4.2])
def dfy(x, y):
    return np.array([0, 1, np.pi*0.7*np.cos(y*np.pi*0.7)/4.2])

X, Y = np.meshgrid(np.linspace(0, 2, 50), np.linspace(0, 2, 50))
Z = f(X, Y)
surf = ax.plot_surface(X, Y, Z, color="green", linewidth=0, rstride=2, cstride=2, antialiased=True, alpha=0.6)
#fig.colorbar(surf)

k = 3
X2, Y2 = np.meshgrid(np.linspace(0, 2, k), np.linspace(0, 2, k))
Z2 = f(X2, Y2)
U = np.zeros((k, k))
V = np.zeros((k, k))
W = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 2, k)):
    for j, y in enumerate(np.linspace(0, 2, k)):
        vec = np.cross(dfx(x, y), dfy(x, y))
        U[j][i] = vec[0]
        V[j][i] = vec[1]
        W[j][i] = vec[2]
ax.scatter(X2, Y2, Z2, color="black", depthshade=False)
ax.quiver(X2, Y2, Z2, U, V, W, length=0.2, normalize=True, color="black")
#1

qed_sol = np.array([[0.75, 0.75, 0.99], [0.3, 1.75, 0.5], [1.7, 1.8, 0.15], [1.75, 0.3, 0.6]])
ax.scatter(qed_sol[:, 0], qed_sol[:, 1], qed_sol[:, 2], color = "red", depthshade=False)
#2

'''on_grid = np.array([[0.50, 0, 0.7],
                    [0, 0, 0.55],
                    [0, 0.40, 0.7],
                    [0, 1.6, 0.4],
                    [0, 2, 0.3],
                    [0.3, 2, 0.4],
                    [1.8, 2, 0.15],
                    [2, 2, 0.15],
                    [2, 1.75, 0.2],
                    [2, 0.50, 0.6],
                    [2, 0, 0.1],
                    [1.60, 0, 0.4]])'''

tmp = np.array([[0.50, 0],
                    [0, 0],
                    [0, 0.60],
                    [0, 1.7],
                    [0, 2],
                    [0.3, 2],
                    [1.8, 2],
                    [2, 2],
                    [2, 1.85],
                    [2, 0.30],
                    [2, 0],
                    [1.60, 0]])
on_grid = np.array([[x+ np.random.random()*0.02,
                     y+ np.random.random()*0.02,
                     f(x, y) + np.random.random()*0.02] for (x,y) in tmp])

faces = [qed_sol,
        [on_grid[2], qed_sol[0], on_grid[0]],
        [on_grid[3], qed_sol[1], qed_sol[0], on_grid[2]],
        [on_grid[5], qed_sol[1], on_grid[3]],
        [on_grid[6], qed_sol[2], qed_sol[1], on_grid[5]],
        [on_grid[8], qed_sol[2], on_grid[6]],
        [on_grid[9], qed_sol[3], qed_sol[2], on_grid[8]],
        [on_grid[11], qed_sol[3], on_grid[9]],
        [on_grid[0], qed_sol[0], qed_sol[3], on_grid[11]]]

for face in faces:
    face = np.array(face)
    ax.plot(face[:, 0], face[:, 1], face[:, 2], color="red")
    """"""
add_box(ax, [0, 0, 0], [1, 1, 1], "black")
add_box(ax, [1, 0, 0], [2, 1, 1], "black")
add_box(ax, [0, 1, 0], [1, 2, 1], "black")
add_box(ax, [1, 1, 0], [2, 2, 1], "black")
ax.set_xlim([0, 2])
ax.set_ylim([0, 2])
ax.set_zlim([0, 2])
#poly.set_alpha(0)
configure_ax(ax)
ax.view_init(30, 20)
plt.show()


print("")
```

```python
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111, projection='3d')

def f1(x, y):
    return np.tanh(1.1*x - 0.5)/2.5 + 0.5
def f2(x, y):
    return np.tanh(-1.1*x + 0.5)/2.5 + 0.4
def df1x(x, y):
    return np.array([1, 0, 1.1*(1 - np.tanh(1.1*x - 0.5)**2)/2.5])
def df1y(x, y):
    return np.array([0, 1, 0])
def df2x(x, y):
    return np.array([1, 0, -1.1*(1 - np.tanh(-1.1*x + 0.5)**2)/2.5])
def df2y(x, y):
    return np.array([0, 1, 0])

X, Y = np.meshgrid(np.linspace(0, 2, 50), np.linspace(0, 2, 50))
Z1 = f1(X, Y)
Z2 = f2(X, Y)
a1, a2 = np.meshgrid(np.linspace(0, 0.3, Z1.shape[0]), np.linspace(0, 0.3, Z1.shape[1]))
my_col = cm.viridis(a1)
surf1 = ax.plot_surface(X, Y, Z1, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
a1, a2 = np.meshgrid(np.linspace(0.7, 1, Z2.shape[0]), np.linspace(0.7, 1, Z2.shape[1]))
my_col = cm.viridis(a1)
surf2 = ax.plot_surface(X, Y, Z2, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
#surf1 = ax.plot_surface(X, Y, Z, , linewidth=0, rstride=2, cstride=2, antialiased=True, alpha=0.6)
#fig.colorbar(surf)

k = 3
_X, _Y = np.meshgrid(np.linspace(0, 2, k), np.linspace(0, 2, k))
_Z1 = f1(_X, _Y)
_Z2 = f2(_X, _Y)
U1 = np.zeros((k, k))
V1 = np.zeros((k, k))
W1 = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 2, k)):
    for j, y in enumerate(np.linspace(0, 2, k)):
        vec = np.cross(df1x(x, y), df1y(x, y))
        U1[j][i] = vec[0]
        V1[j][i] = vec[1]
        W1[j][i] = vec[2]
U2 = np.zeros((k, k))
V2 = np.zeros((k, k))
W2 = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 2, k)):
    for j, y in enumerate(np.linspace(0, 2, k)):
        vec = np.cross(df2x(x, y), df2y(x, y))
        U2[j][i] = vec[0]
        V2[j][i] = vec[1]
        W2[j][i] = vec[2]
if True:
    ax.scatter(_X, _Y, _Z1, color="black", depthshade=False)
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="black")
    ax.scatter(_X, _Y, _Z2, color="black", depthshade=False)
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="black")
else:
    ax.scatter(_X, _Y, _Z1, color="purple", depthshade=False, marker="s")
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="purple")
    ax.scatter(_X, _Y, _Z2, color="green", depthshade=False, marker="D")
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="green")
    
qed_sol = np.array([[0.3, 0.5, 0.45],
                    [0.3, 1.5, 0.5],
                    [1.5, 1.5, 0.85],
                    [1.5, 0.5, 0.85],
                    [1.5, 1.5, 0.05],
                    [1.5, 0.5, 0.05]])
ax.scatter(qed_sol[:, 0], qed_sol[:, 1], qed_sol[:, 2], color = "red", depthshade=False)

if False:
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 1), (1, 0)]),
         (qed_sol[1], [(1, 0), (1, 1), (2, 1), (2, 0)]),
         (qed_sol[2], [(1, 1), (1, 2), (2, 2), (2, 1)]),
         (qed_sol[3], [(0, 1), (0, 2), (1, 2), (1, 1)])
        ]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z1[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z1[idx]],
                     color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
            
          
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 1), (1, 0)]),
         (qed_sol[1], [(1, 0), (1, 1), (2, 1), (2, 0)]),
         (qed_sol[4], [(1, 1), (1, 2), (2, 2), (2, 1)]),
         (qed_sol[5], [(0, 1), (0, 2), (1, 2), (1, 1)])
        ]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z2[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z2[idx]],
                      color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
       
tmp = [(0.3, 0),
       (0, 0),
       (0, 0.5),
       (0, 1.5),
       (0, 2),
       (0.3, 2),
       (1.5, 2),
       (2, 2),
       (2, 1.5),
       (2, 0.5),
       (2, 0),
       (1.5, 0)]
on_grid1 = np.array([[x+ np.random.random()*0.02,
                     y+ np.random.random()*0.02,
                     f1(x, y) + np.random.random()*0.02] for (x,y) in tmp])
on_grid2 = np.array([[x+ np.random.random()*0.02,
                     y+ np.random.random()*0.02,
                     f2(x, y) + np.random.random()*0.02] for x,y in tmp])

faces = [[qed_sol[0], qed_sol[1], qed_sol[2], qed_sol[3], qed_sol[0]],
         [on_grid1[2], qed_sol[0], on_grid1[0]],
         [on_grid1[3], qed_sol[1], qed_sol[0], on_grid1[2]],
         [on_grid1[5], qed_sol[1], on_grid1[3]],
         [on_grid1[6], qed_sol[2], qed_sol[1], on_grid1[5]],
         [on_grid1[8], qed_sol[2], on_grid1[6]],
         [on_grid1[9], qed_sol[3], qed_sol[2], on_grid1[8]],
         [on_grid1[11], qed_sol[3], on_grid1[9]],
         [on_grid1[0], qed_sol[0], qed_sol[3], on_grid1[11]], #
         [qed_sol[0], qed_sol[1], qed_sol[4], qed_sol[5], qed_sol[0]],
         [on_grid2[6], qed_sol[4], qed_sol[1], on_grid1[5]],
         [on_grid2[8], qed_sol[4], on_grid2[6]],
         [on_grid2[9], qed_sol[5], qed_sol[4], on_grid2[8]],
         [on_grid2[11], qed_sol[5], on_grid2[9]],
         [on_grid1[0], qed_sol[0], qed_sol[5], on_grid2[11]],
         [on_grid2[2], qed_sol[0]],
         [on_grid2[3], qed_sol[1]],
        ]

for face in faces:
    face = np.array(face)
    ax.plot(face[:, 0], face[:, 1], face[:, 2], color="red")
""""""  
add_box(ax, [0, 0, 0], [1, 1, 1], "black")
add_box(ax, [1, 0, 0], [2, 1, 1], "black")
add_box(ax, [0, 1, 0], [1, 2, 1], "black")
add_box(ax, [1, 1, 0], [2, 2, 1], "black")
ax.set_xlim([0, 2])
ax.set_ylim([0, 2])
ax.set_zlim([0, 2])

#poly.set_alpha(0)
configure_ax(ax)
ax.view_init(30, -60)
plt.show()

```

```python
fig = plt.figure(figsize=(3.5, 3.5))
ax = fig.add_subplot(111, projection='3d')

X, Y = np.meshgrid(np.linspace(0, 1, 50), np.linspace(0, 1, 50))
Z1 = f1(X, Y)
Z2 = f2(X, Y)
a1, a2 = np.meshgrid(np.linspace(0, 0.15, Z1.shape[0]), np.linspace(0, 0.15, Z1.shape[1]))
my_col = cm.viridis(a1)
surf1 = ax.plot_surface(X, Y, Z1, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
a1, a2 = np.meshgrid(np.linspace(0.7, 0.85, Z2.shape[0]), np.linspace(0.7, 0.85, Z2.shape[1]))
my_col = cm.viridis(a1)
surf2 = ax.plot_surface(X, Y, Z2, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
#surf1 = ax.plot_surface(X, Y, Z, , linewidth=0, rstride=2, cstride=2, antialiased=True, alpha=0.6)
#fig.colorbar(surf)

k = 2
_X, _Y = np.meshgrid(np.linspace(0, 1, k), np.linspace(0, 1, k))
_Z1 = f1(_X, _Y)
_Z2 = f2(_X, _Y)
U1 = np.zeros((k, k))
V1 = np.zeros((k, k))
W1 = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 1, k)):
    for j, y in enumerate(np.linspace(0, 1, k)):
        vec = np.cross(df1x(x, y), df1y(x, y))
        U1[j][i] = vec[0]
        V1[j][i] = vec[1]
        W1[j][i] = vec[2]
U2 = np.zeros((k, k))
V2 = np.zeros((k, k))
W2 = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 1, k)):
    for j, y in enumerate(np.linspace(0, 1, k)):
        vec = np.cross(df2x(x, y), df2y(x, y))
        U2[j][i] = vec[0]
        V2[j][i] = vec[1]
        W2[j][i] = vec[2]
if True:
    ax.scatter(_X, _Y, _Z1, color="blue", depthshade=False, marker="*")
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="blue")
    ax.scatter(_X, _Y, _Z2, color="blue", depthshade=False, marker="*")
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="blue")
else:
    ax.scatter(_X, _Y, _Z1, color="purple", depthshade=False, marker="s")
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="purple")
    ax.scatter(_X, _Y, _Z2, color="green", depthshade=False, marker="D")
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="green")


qed_sol = np.array([[0.3, 0.5, 0.45]])
ax.scatter(qed_sol[:, 0], qed_sol[:, 1], qed_sol[:, 2], color = "blue", depthshade=False)

if True:
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 1), (1, 0)])]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z1[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z1[idx]],
                     color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
            
          
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 1), (1, 0)])]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z2[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z2[idx]],
                      color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
          
""""""
add_box(ax, [0, 0, 0], [1, 1, 1], "black")
ax.set_xlim([0, 1])
ax.set_ylim([0, 1])
ax.set_zlim([0, 1])

#poly.set_alpha(0)
configure_ax(ax)
ax.view_init(30, -60)
plt.show()

```

```python
fig = plt.figure(figsize=(3.5, 3.5))
ax = fig.add_subplot(111, projection='3d')

X, Y = np.meshgrid(np.linspace(1, 2, 50), np.linspace(0, 1, 50))
Z1 = f1(X, Y)
Z2 = f2(X, Y)
a1, a2 = np.meshgrid(np.linspace(0.15, 0.30, Z1.shape[0]), np.linspace(0.15, 0.30, Z1.shape[1]))
my_col = cm.viridis(a1)
surf1 = ax.plot_surface(X, Y, Z1, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
a1, a2 = np.meshgrid(np.linspace(0.85, 1, Z2.shape[0]), np.linspace(0.85, 1, Z2.shape[1]))
my_col = cm.viridis(a1)
surf2 = ax.plot_surface(X, Y, Z2, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
#surf1 = ax.plot_surface(X, Y, Z, , linewidth=0, rstride=2, cstride=2, antialiased=True, alpha=0.6)
#fig.colorbar(surf)

k = 2
_X, _Y = np.meshgrid(np.linspace(1, 2, k), np.linspace(0, 1, k))
_Z1 = f1(_X, _Y)
_Z2 = f2(_X, _Y)
U1 = np.zeros((k, k))
V1 = np.zeros((k, k))
W1 = np.zeros((k, k))
for i, x in enumerate(np.linspace(1, 2, k)):
    for j, y in enumerate(np.linspace(0, 1, k)):
        vec = np.cross(df1x(x, y), df1y(x, y))
        U1[j][i] = vec[0]
        V1[j][i] = vec[1]
        W1[j][i] = vec[2]
U2 = np.zeros((k, k))
V2 = np.zeros((k, k))
W2 = np.zeros((k, k))
for i, x in enumerate(np.linspace(1, 2, k)):
    for j, y in enumerate(np.linspace(0, 1, k)):
        vec = np.cross(df2x(x, y), df2y(x, y))
        U2[j][i] = vec[0]
        V2[j][i] = vec[1]
        W2[j][i] = vec[2]
if False:
    ax.scatter(_X, _Y, _Z1, color="blue", depthshade=False, marker="*")
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="blue")
    ax.scatter(_X, _Y, _Z2, color="blue", depthshade=False, marker="*")
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="blue")
else:
    ax.scatter(_X, _Y, _Z1, color="purple", depthshade=False, marker="s")
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="purple")
    ax.scatter(_X, _Y, _Z2, color="green", depthshade=False, marker="D")
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="green")


qed_sol = np.array([[1.5, 0.5, 0.85],
                    [1.5, 0.5, 0.05]])
ax.scatter(qed_sol[0, 0], qed_sol[0, 1], qed_sol[0, 2], color = "purple", depthshade=False)
ax.scatter(qed_sol[1, 0], qed_sol[1, 1], qed_sol[1, 2], color = "green", depthshade=False)

if True:
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 0), (1, 1)])]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z1[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z1[idx]],
                     color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
            
          
    l = [(qed_sol[1], [(0, 0), (0, 1), (1, 0), (1, 1)])]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z2[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z2[idx]],
                      color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
          
""""""
add_box(ax, [1, 0, 0], [2, 1, 1], "black")
ax.set_xlim([1, 2])
ax.set_ylim([0, 1])
ax.set_zlim([0, 1])

#poly.set_alpha(0)
configure_ax(ax)
ax.view_init(30, -60)
plt.show()

```

```python
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111, projection='3d')

def add_full_box(ax, cinf, csup, color, alpha):
    vertices = np.array([
                    [cinf[0], cinf[1], cinf[2]],
                    [csup[0], cinf[1], cinf[2]],
                    [csup[0], csup[1], cinf[2]],
                    [cinf[0], csup[1], cinf[2]],
                    [cinf[0], cinf[1], csup[2]],
                    [csup[0], cinf[1], csup[2]],
                    [csup[0], csup[1], csup[2]],
                    [cinf[0], csup[1], csup[2]]
                ])
    faces = [
                [vertices[0],vertices[1],vertices[2],vertices[3]],
                [vertices[4],vertices[5],vertices[6],vertices[7]],
                [vertices[0],vertices[1],vertices[5],vertices[4]],
                [vertices[2],vertices[3],vertices[7],vertices[6]],
                [vertices[1],vertices[2],vertices[6],vertices[5]],
                [vertices[4],vertices[7],vertices[3],vertices[0]]
            ]
    #ax.scatter(vertices[:, 0], vertices[:, 1], vertices[:, 2], alpha=0, color="black")
    ax.add_collection3d(Poly3DCollection(faces, facecolors=color, linewidths=0, alpha=alpha, zsort="max"))

a = np.linspace(-1.35, 1.35, 100)

def f(a):
    return np.array([a**2 - 1, a*(a-1)*(a+1), a*0.5 + 1])

X = a**2 - 1
Y = a*(a-1)*(a+1)
Z = a*0.5 + 1

ax.plot(X, Y, Z, color="green")
pts = np.array([f(np.sqrt(0.5)), f(-np.sqrt(0.5)), f(np.sqrt(1.42)), f(-np.sqrt(1.42))])
ax.scatter(pts[:, 0], pts[:, 1], pts[:, 2], color="green", depthshade=False)

ax.plot([-0.5, -0.5, 0.5, 0.5, -0.5], [-0.5, 0.5, 0.5, -0.5, -0.5], [0 for i in range(5)], color="black",
        linewidth=1)
for x, y in [(-0.5, -0.5), (-0.5, 0.5), (0.5, 0.5), (0.5, -0.5)]:
    ax.plot([x, x], [y, y], [0, 2], color="black", linestyle="--")
add_full_box(ax, [-0.5, -0.5, 0], [0.5, 0.5, 2], "red", 0.1)



ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_zlim([0, 2])

#poly.set_alpha(0)
#configure_ax(ax)
#ax.view_init(30, -60)
plt.show()
```

`````python
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111, projection='3d')

def f1(x, y):
    return np.tanh(1.1*x - 0.5)/2.5 + 0.5
def f2(x, y):
    return np.tanh(-1.1*x + 0.5)/2.5 + 0.4
def df1x(x, y):
    return np.array([1, 0, 1.1*(1 - np.tanh(1.1*x - 0.5)**2)/2.5])
def df1y(x, y):
    return np.array([0, 1, 0])
def df2x(x, y):
    return np.array([1, 0, -1.1*(1 - np.tanh(-1.1*x + 0.5)**2)/2.5])
def df2y(x, y):
    return np.array([0, 1, 0])

X, Y = np.meshgrid(np.linspace(0, 2, 50), np.linspace(0, 2, 50))
Z1 = f1(X, Y)
Z2 = f2(X, Y)
a1, a2 = np.meshgrid(np.linspace(0, 0.3, Z1.shape[0]), np.linspace(0, 0.3, Z1.shape[1]))
my_col = cm.viridis(a1)
surf1 = ax.plot_surface(X, Y, Z1, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
a1, a2 = np.meshgrid(np.linspace(0.7, 1, Z2.shape[0]), np.linspace(0.7, 1, Z2.shape[1]))
my_col = cm.viridis(a1)
surf2 = ax.plot_surface(X, Y, Z2, facecolors = my_col, linewidth=0, antialiased=True, rstride=2, cstride=2, alpha=0.6)
#surf1 = ax.plot_surface(X, Y, Z, , linewidth=0, rstride=2, cstride=2, antialiased=True, alpha=0.6)
#fig.colorbar(surf)

k = 3
_X, _Y = np.meshgrid(np.linspace(0, 2, k), np.linspace(0, 2, k))
_Z1 = f1(_X, _Y)
_Z2 = f2(_X, _Y)
U1 = np.zeros((k, k))
V1 = np.zeros((k, k))
W1 = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 2, k)):
    for j, y in enumerate(np.linspace(0, 2, k)):
        vec = np.cross(df1x(x, y), df1y(x, y))
        U1[j][i] = vec[0]
        V1[j][i] = vec[1]
        W1[j][i] = vec[2]
U2 = np.zeros((k, k))
V2 = np.zeros((k, k))
W2 = np.zeros((k, k))
for i, x in enumerate(np.linspace(0, 2, k)):
    for j, y in enumerate(np.linspace(0, 2, k)):
        vec = np.cross(df2x(x, y), df2y(x, y))
        U2[j][i] = vec[0]
        V2[j][i] = vec[1]
        W2[j][i] = vec[2]
if True:
    ax.scatter(_X, _Y, _Z1, color="black", depthshade=False)
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="black")
    ax.scatter(_X, _Y, _Z2, color="black", depthshade=False)
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="black")
else:
    ax.scatter(_X, _Y, _Z1, color="purple", depthshade=False, marker="s")
    ax.quiver(_X, _Y, _Z1, U1, V1, W1, length=0.2, normalize=True, color="purple")
    ax.scatter(_X, _Y, _Z2, color="green", depthshade=False, marker="D")
    ax.quiver(_X, _Y, _Z2, U2, V2, W2, length=0.2, normalize=True, color="green")
    
qed_sol = np.array([[0.3, 0.5, 0.45],
                    [0.3, 1.5, 0.5],
                    [1.5, 1.5, 0.85],
                    [1.5, 0.5, 0.85],
                    [1.5, 1.5, 0.05],
                    [1.5, 0.5, 0.05]])
ax.scatter(qed_sol[:, 0], qed_sol[:, 1], qed_sol[:, 2], color = "red", depthshade=False)

if False:
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 1), (1, 0)]),
         (qed_sol[1], [(1, 0), (1, 1), (2, 1), (2, 0)]),
         (qed_sol[2], [(1, 1), (1, 2), (2, 2), (2, 1)]),
         (qed_sol[3], [(0, 1), (0, 2), (1, 2), (1, 1)])
        ]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z1[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z1[idx]],
                     color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
            
          
    l = [(qed_sol[0], [(0, 0), (0, 1), (1, 1), (1, 0)]),
         (qed_sol[1], [(1, 0), (1, 1), (2, 1), (2, 0)]),
         (qed_sol[4], [(1, 1), (1, 2), (2, 2), (2, 1)]),
         (qed_sol[5], [(0, 1), (0, 2), (1, 2), (1, 1)])
        ]
    for pt, lidx in l:
        for idx in lidx:
            ax.quiver([_X[idx]], [_Y[idx]], [_Z2[idx]], [pt[0] - _X[idx]], [pt[1] - _Y[idx]], [pt[2] - _Z2[idx]],
                      color = "black", alpha = 0.8, length=0.5, linestyle="--")
            #ax.plot([_X[idx], pt[0]], [_Y[idx], pt[1]], [_Z1[idx], pt[2]], color = "black", alpha = 0.8)
       
tmp = [(0.3, 0),
       (0, 0),
       (0, 0.5),
       (0, 1.5),
       (0, 2),
       (0.3, 2),
       (1.5, 2),
       (2, 2),
       (2, 1.5),
       (2, 0.5),
       (2, 0),
       (1.5, 0)]
on_grid1 = np.array([[x+ np.random.random()*0.02,
                     y+ np.random.random()*0.02,
                     f1(x, y) + np.random.random()*0.02] for (x,y) in tmp])
on_grid2 = np.array([[x+ np.random.random()*0.02,
                     y+ np.random.random()*0.02,
                     f2(x, y) + np.random.random()*0.02] for x,y in tmp])

faces = [[qed_sol[0], qed_sol[1], qed_sol[2], qed_sol[3], qed_sol[0]],
         [on_grid1[2], qed_sol[0], on_grid1[0]],
         [on_grid1[3], qed_sol[1], qed_sol[0], on_grid1[2]],
         [on_grid1[5], qed_sol[1], on_grid1[3]],
         [on_grid1[6], qed_sol[2], qed_sol[1], on_grid1[5]],
         [on_grid1[8], qed_sol[2], on_grid1[6]],
         [on_grid1[9], qed_sol[3], qed_sol[2], on_grid1[8]],
         [on_grid1[11], qed_sol[3], on_grid1[9]],
         [on_grid1[0], qed_sol[0], qed_sol[3], on_grid1[11]], #
         [qed_sol[0], qed_sol[1], qed_sol[4], qed_sol[5], qed_sol[0]],
         [on_grid2[6], qed_sol[4], qed_sol[1], on_grid1[5]],
         [on_grid2[8], qed_sol[4], on_grid2[6]],
         [on_grid2[9], qed_sol[5], qed_sol[4], on_grid2[8]],
         [on_grid2[11], qed_sol[5], on_grid2[9]],
         [on_grid1[0], qed_sol[0], qed_sol[5], on_grid2[11]],
         [on_grid2[2], qed_sol[0]],
         [on_grid2[3], qed_sol[1]],
        ]

for face in faces:
    face = np.array(face)
    ax.plot(face[:, 0], face[:, 1], face[:, 2], color="red")
""" 
add_box(ax, [0, 0, 0], [1, 1, 1], "black")
add_box(ax, [1, 0, 0], [2, 1, 1], "black")
add_box(ax, [0, 1, 0], [1, 2, 1], "black")
add_box(ax, [1, 1, 0], [2, 2, 1], "black")
ax.set_xlim([0, 2])
ax.set_ylim([0, 2])
ax.set_zlim([0, 2])

#poly.set_alpha(0)
#configure_ax(ax)
#ax.view_init(30, -60)
plt.show()

```
````
`````
