---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.1
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import numpy as np
import mpl_toolkits.mplot3d as a3
import matplotlib.pyplot as plt
import ibex_parser
```

```python
data = ibex_parser.read_ibex_cov_file("../data/maple_xy.cov")
```

```python
for key, value in data.items():
    print(f"{key} : {value}")
```

```python
len(data["solution_boxes"]) == len(data["boundary_boxes"])
```

```python
%matplotlib widget
fig = plt.figure(figsize=(7, 7))
ax = fig.add_subplot(111, projection='3d')

for i, box in enumerate(data["boxes"]):
    point = (box[0] + box[1]) / 2
    if i in data["solution_boxes"]:
        color = 'green'
    elif i in data["boundary_boxes"]:
        color = 'blue'
    elif i in data["pending_boxes"]:
        color = 'grey'
    else:
        color = 'red'

    ax.scatter(point[0], point[1], point[2], color=color)

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1)
plt.show()
```

```python
import matplotlib.pyplot as plt
%matplotlib widget
plt.plot([1, 2, 3], [4, 5, 6])
plt.show()
```

```python
import numpy as np

```

```python
a = np.array([[1, 2, 2], [4, 5, 6]])
a[:,0]
```

```python
import numpy as np
a = np.array([[0.21052631579243347, 3.6371505913230466, 3.5789473684210531],
[-0.06049774624993523, 4, 3.5789473684210531],
[0.21052630392754018, 4.0000000086250953, 3.840457707705895]])

print(a.sum(axis=0))

```

```python
import numpy as np
x = np.array([0.34483, 0.62069, -0.2069, 0.2069])
(x+2)*29/4
```

```python
from math import sqrt
sqrt(0.6206896)
```

```python
-0.2068965/0.7878385

```

```python

```
