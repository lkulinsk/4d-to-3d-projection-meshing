input_mbx_file = "../data/mbx/whitney_umbrella.mbx"
input_cov_file = ""
output_cov_file = ""
output_ply_file = "../data/ply/aaa.ply"

resolution = { 99 ; 99 ; 99 }
ibex_eps_max = 1e-3
ibex_eps_min = 1e-2
grid_in_mesh = false
ball_system_threshold = 1e-3