// This first include is essential to make CGAL and Ibex rounding modes compatible. However, the hack behind it may cause an issue one day.
//#include "upward_rounding.h"
#include "dual_contouring.h"
#include "types.h"
#include "ibex_utilities.cpp"
#include "ply_utilities.cpp"
#include "param_parser.cpp"
#include <iomanip>
#include <ibex.h>
#include <algorithm>
#include <utility>
#include <memory>
#include <array>
#include <string>
#include <vector>
#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <chrono>
#include <fstream>
#include <Eigen/Dense>
#include "utilities.cpp"
#include <stack>
#include <cmath>

std::vector<Index_3> get_adjacent_voxels(const EdgeIndex_3& edge, const Dimension& res)
{
    // NB : L'ordre des indices renvoyés a une importance pour la quadrangulation finale. En effet, quand ply lit une face quad 0 1 2 3, il considère qu'elle est faite des triangles 0 1 2 et 0 2 3, et pas 0 1 2 et 1 2 3.
    auto [pt, dir] = edge;
    std::vector<Index_3> neighbours {};
    switch(dir)
    {
        case PX: // Le PX implique 0 <= pt[0] < res[0]
            if(pt[1] > 0 && pt[2] > 0)
                neighbours.push_back({pt[0], pt[1] - 1, pt[2] - 1});
            if(pt[1] > 0 && pt[2] < res[1])
                neighbours.push_back({pt[0], pt[1] - 1, pt[2]});
            if(pt[1] < res[0] && pt[2] < res[1])
                neighbours.push_back({pt[0], pt[1], pt[2]});
            if(pt[1] < res[0] && pt[2] > 0)
                neighbours.push_back({pt[0], pt[1], pt[2] - 1});
            break;
        case PY: // Le PY implique 0 <= pt[1] < res[1]
            if(pt[0] > 0 && pt[2] > 0)
                neighbours.push_back({pt[0] - 1, pt[1], pt[2] - 1});
            if(pt[0] > 0 && pt[2] < res[1])
                neighbours.push_back({pt[0] - 1, pt[1], pt[2]});
            if(pt[0] < res[0] && pt[2] < res[1])
                neighbours.push_back({pt[0], pt[1], pt[2]});
            if(pt[0] < res[0] && pt[2] > 0)
                neighbours.push_back({pt[0], pt[1], pt[2] - 1});
            break;
        case PZ: // Le PZ implique 0 <= pt[2] < res[2]
            if(pt[0] > 0 && pt[1] > 0)
                neighbours.push_back({pt[0] - 1, pt[1] - 1, pt[2]});
            if(pt[0] > 0 && pt[1] < res[1])
                neighbours.push_back({pt[0] - 1, pt[1], pt[2]});
            if(pt[0] < res[0] && pt[1] < res[1])
                neighbours.push_back({pt[0], pt[1], pt[2]});
            if(pt[0] < res[0] && pt[1] > 0)
                neighbours.push_back({pt[0], pt[1] - 1, pt[2]});
            break;
    }
    return neighbours;
}

std::vector<EdgeIndex_3> get_adjacent_edges(const Index_3& idx)
{
    return {
        {{idx[0], idx[1], idx[2]}, PX},
        {{idx[0], idx[1], idx[2]}, PY},
        {{idx[0], idx[1], idx[2]}, PZ},
        {{idx[0] + 1, idx[1], idx[2]}, PY},
        {{idx[0] + 1, idx[1], idx[2]}, PZ},
        {{idx[0], idx[1] + 1, idx[2]}, PX},
        {{idx[0], idx[1] + 1, idx[2]}, PZ},
        {{idx[0], idx[1], idx[2] + 1}, PX},
        {{idx[0], idx[1], idx[2] + 1}, PY},
        {{idx[0], idx[1] + 1, idx[2] + 1}, PX},
        {{idx[0] + 1, idx[1], idx[2] + 1}, PY},
        {{idx[0] + 1, idx[1] + 1, idx[2]}, PZ}
    };
}

bool point_in_voxel_bound(const Vector_3& pt, const Point_3& corner_inf, const Point_3& corner_sup)
{
    return (corner_inf.array() <= pt.array()).all() && (pt.array() <= corner_sup.array()).all();
}

Vector_3 voxel_segment_intersection(const Point_3& A, const Point_3& B, const Point_3& corner_inf, const Point_3& corner_sup)
{
    // Le segment AB a l'extrémité A dans le voxel, l'extrémité B a l'extérieur.
    // On renvoie le point d'intersection (garanti) voxel/segment
    Vector_3 AB = B - A;
    Vector_3 P1 = corner_inf - A; // P est un point sur le plan.
    Vector_3 P2 = corner_sup - A;
    std::vector<double> lambdas = {
        P1[0] / AB[0],
        P1[1] / AB[1],
        P1[2] / AB[2],
        P2[0] / AB[0],
        P2[1] / AB[1],
        P2[2] / AB[2]
    };
    double min_lamb = 2;
    for(auto lamb : lambdas)
        if(0 <= lamb && lamb <= 1 && lamb <= min_lamb)
            min_lamb = lamb;
    //Sanity check for now
    if(min_lamb > 1)
    {
        min_lamb = -2;
        for(auto lamb : lambdas)
            if(0 >= lamb && lamb >= -1 && lamb >= min_lamb)
                min_lamb = lamb;
        if(min_lamb < -1)
            std::cout << "min_lamb problème.\n"; 
    }
    return A + min_lamb * AB;
}

template <class T>
Matrix_3<T> initialize(const Dimension& resolution, const T& init_value)
{
    return Matrix_3<T>(resolution[0], Matrix_2<T>(resolution[1], Matrix_1<T>(resolution[2], init_value)));
}

Point_3 index_to_coord(const std::array<double, 3>& idx, const Point_3& corner_inf, const Point_3& corner_sup, const Dimension& resolution)
{
    Point_3 pt = Point_3::Zero();
    for(std::size_t i = 0; i < 3; ++i)
        pt[i] = corner_inf[i] + idx[i] / resolution[i] * (corner_sup[i] - corner_inf[i]);
    return pt;
}

//Returns the grid cells idx enclosing the box (lower bound included, upper bound not included)
std::array<Index_3, 2> box_to_grid_index(const ibex::IntervalVector& box, const Point_3& corner_inf, const Point_3& corner_sup, const Dimension& resolution)
{
    Index_3 index_inf = {0, 0, 0};
    Index_3 index_sup = {0, 0, 0};
    for(std::size_t i = 0; i < 3; ++i)
    {
        index_inf[i] = std::floor((box[i].lb() - corner_inf[i]) / (corner_sup[i] - corner_inf[i]) * resolution[i]);
        index_inf[i] = std::max(0, index_inf[i]);
        index_sup[i] = std::floor((box[i].ub() - corner_inf[i]) / (corner_sup[i] - corner_inf[i]) * resolution[i]) + 1;
        index_sup[i] = std::min(resolution[i], index_sup[i]);
    }
    return {index_inf, index_sup};
}

ibex::IntervalVector intersect_boxes(const ibex::IntervalVector& box1, const ibex::IntervalVector& box2)
{
    //std::cout << box1 << " " << box2 << "\n";
    assert(box1.size() == box2.size());
    if(!box1.intersects(box2))
        return ibex::IntervalVector::empty(box1.size());
    ibex::IntervalVector sol(box1.size());
    for(int i = 0; i < box1.size(); ++i)
        sol[i] = ibex::Interval {std::max(box1.lb()[i], box2.lb()[i]), std::min(box1.ub()[i], box2.ub()[i])};
    return sol;
}

Vector_3 get_normal(const ibex::Function& dfg, const ibex::Vector& pt)
{
    // dfg est la matrice jacobienne de u -> (f(u), g(u))
    ibex::Matrix jac = dfg.eval_matrix(pt).mid();
    // TODO: Il ne faudrait pas plutôt examiner si le zéro est dans l'intervalle / tout faire avec des intervalles jusqu'au dernier moment ?
    Vector_3 l1 {jac[0][0], jac[0][1], jac[0][2]};
    Vector_3 l2 {jac[1][0], jac[1][1], jac[1][2]};
    if(jac[0][3] == 0)
        return l1;
    else if(jac[1][3] == 0)
        return l2;
    else
    {
        double factor = jac[0][3]/jac[1][3];
        Vector_3 normal {l1[0]-l2[0]*factor, l1[1]-l2[1]*factor, l1[2]-l2[2]*factor};
        return normal.normalized(); // Norme nulle possible ?
    }
}

Vector_3 get_normal(const ibex::Function& dfg, const Point_3& pt)
{
    return get_normal(dfg, ibex::Vector {pt[0], pt[1], pt[2]});
}

ibex::IntervalVector make_interval_from_segment(const Point_3& a, const Point_3& b, const ibex::Interval& t_range)
{
    return {ibex::Interval(a[0], b[0]),
    ibex::Interval(a[1], b[1]),
    ibex::Interval(a[2], b[2]),
    t_range};
}

Point_3 solve_QED(const std::vector<Point_3>& adj_intersections, const std::vector<Point_3>& adj_normals, const Point_3& voxel_corner_inf, const Point_3& voxel_corner_sup)
{
    //Computing the barycenter
    Vector_3 barycenter = Vector_3::Zero();
    for(auto& pt : adj_intersections)
        barycenter += pt;
    barycenter /= adj_intersections.size();

    //Create matrices for the QED
    Eigen::MatrixX3d A(adj_intersections.size(), 3);
    Eigen::VectorXd B(adj_intersections.size());
    for(size_t i = 0; i < adj_intersections.size(); ++i)
    {
        for(size_t j = 0; j < 3; ++j)
            A(i, j) = adj_normals[i][j];
        B(i) = adj_normals[i].dot(adj_intersections[i]);
    }
    //Crops the eigenvalues which when inverted are too big (it means the normals are almost colinear)
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(A.transpose()*A, Eigen::ComputeFullU | Eigen::ComputeFullV);
    Vector_3 eigenvalues = svd.singularValues();
    Vector_3 inveigencrop = Vector_3::Zero();
    for(int i = 0; i < 3; i++)
    {
        if(eigenvalues[i] < 0.1)
            inveigencrop[i] = 0;
        else
            inveigencrop[i] = 1 / eigenvalues(i);
    }

    //Solves the QED.
    Vector_3 x = svd.matrixV() * inveigencrop.asDiagonal() * svd.matrixU().transpose() * A.transpose() * (B - A * barycenter) + barycenter;;

    //If the QED solution lies outside the voxel, take the barycenter instead.
    if(!point_in_voxel_bound(x, voxel_corner_inf, voxel_corner_sup))
        x = barycenter;
    
    return x;
}

void add_grid_to_mesh(Mesh& mesh, std::vector<Point_3>& points, const Matrix_3<VertexData>& vertices_grid, const Dimension& nb_vertices)
{
    int starting_id = points.size();
    for(int i = 0; i < nb_vertices[0]; ++i)
        for(int j = 0; j < nb_vertices[1]; ++j)
            for(int k = 0; k < nb_vertices[2]; ++k)
            {
                points.push_back(vertices_grid[i][j][k].coord);
            }
    for(int i = 0; i < nb_vertices[0]; ++i)
        for(int j = 0; j < nb_vertices[1]; ++j)
            for(int k = 0; k < nb_vertices[2] - 1; ++k)
            {
                Face edge = {starting_id + nb_vertices[1]*nb_vertices[2]*i + nb_vertices[2]*j + k,
                starting_id + nb_vertices[1]*nb_vertices[2]*i + nb_vertices[2]*j + (k+1)};
                mesh.push_back(edge);
            }
    for(int i = 0; i < nb_vertices[0]; ++i)
        for(int j = 0; j < nb_vertices[1] - 1; ++j)
            for(int k = 0; k < nb_vertices[2] ; ++k)
            {
                Face edge = {starting_id + nb_vertices[1]*nb_vertices[2]*i + nb_vertices[2]*j + k,
                starting_id + nb_vertices[1]*nb_vertices[2]*i + nb_vertices[2]*(j+1) + k};
                mesh.push_back(edge);
            }
    for(int i = 0; i < nb_vertices[0] - 1; ++i)
        for(int j = 0; j < nb_vertices[1]; ++j)
            for(int k = 0; k < nb_vertices[2]; ++k)
            {
                Face edge = {starting_id + nb_vertices[1]*nb_vertices[2]*i + nb_vertices[2]*j + k,
                starting_id + nb_vertices[1]*nb_vertices[2]*(i+1) + nb_vertices[2]*j + k};
                mesh.push_back(edge);
            }
}

int main()
{
    //0. Initialisation.
    std::cout << "\nDual contouring !\n\nInitialization...\n";

    std::string params_path = "../src/param.txt";

    std::string input_mbx_path, input_cov_file, output_cov_path, output_mbx_path;
    Dimension resolution;
    double ibex_eps_max, ibex_eps_min, ball_system_threshold;
    bool grid_in_mesh = false;
    parse_params(params_path, input_mbx_path, input_cov_file, output_cov_path, output_mbx_path, resolution, ibex_eps_max, ibex_eps_min, ball_system_threshold, grid_in_mesh);

    ibex::System system(input_mbx_path.c_str());
    ibex::Function dfg = system.f_ctrs.diff(); // Jacobienne de u -> (f(u), g(u))
    ibex::DefaultSolver solver(system, ibex_eps_min, ibex_eps_max);
    ibex::Interval t_range = system.box[3];
    CustomIbexCov final_cov;
    
    //Transformer ci dessous en IntervalVector (plus cohérent avec ibex)
    Point_3 corner_inf = {system.box[0].lb(), system.box[1].lb(), system.box[2].lb()};
    Point_3 corner_sup = {system.box[0].ub(), system.box[1].ub(), system.box[2].ub()};

    VoxelsMap voxels_map {};
    EdgesMap edges_map {};
    Dimension nb_vertices = {resolution[0] + 1, resolution[1] + 1, resolution[2] + 1};
    Matrix_3<VertexData> vertices_grid {initialize<VertexData>(nb_vertices, VertexData())};


    // 1. Evaluer la fonction en chaque point de la grille.
    for(int i = 0; i < nb_vertices[0]; i++)
        for(int j = 0; j < nb_vertices[1]; j++)
            for(int k = 0; k < nb_vertices[2]; k++)
                vertices_grid[i][j][k].coord = index_to_coord({static_cast<double>(i), static_cast<double>(j), static_cast<double>(k)}, corner_inf, corner_sup, resolution);

    std::cout << "\nComputing grid / surface intersections...\n";
    auto start = std::chrono::high_resolution_clock::now();
    // 2. Calcul les boîtes ibex pour chaque segment maximal de la grille.
    for(int i = 0; i < nb_vertices[0]; ++i)
        for(int j = 0; j < nb_vertices[1]; ++j)
        {
            solver.solve(make_interval_from_segment(vertices_grid[i][j][0].coord, vertices_grid[i][j][nb_vertices[2] - 1].coord, t_range));
            const ibex::CovSolverData& cov = solver.get_data();
            final_cov.combine_cov(cov);
            // En première approx, on traite toutes les boites peu importe leur nature.
            // On commence par transformer les boites en point, et les mettre dans un vecteur.
            std::vector<std::pair<ibex::IntervalVector, ibex::Vector>> boxes_mid;
            boxes_mid.reserve(cov.size());
            for(size_t ibox = 0; ibox < cov.size(); ++ibox)
                boxes_mid.push_back({cov[ibox], cov[ibox].mid()});

            // On les trie pour simplifier le processus d'assignation aux arêtes.
            auto key = [](const std::pair<ibex::IntervalVector, ibex::Vector>& a, const std::pair<ibex::IntervalVector, ibex::Vector>& b) {return a.second[2] < b.second[2];};
            std::sort(boxes_mid.begin(), boxes_mid.end(), key);
            
            size_t ipt = 0;
            for(int k = 0; k < nb_vertices[2] - 1; ++k)
            {
                std::vector<ibex::IntervalVector> intersections {};
                while((ipt < boxes_mid.size()) && (vertices_grid[i][j][k].coord[2] <= boxes_mid[ipt].second[2]) && (boxes_mid[ipt].second[2] < vertices_grid[i][j][k+1].coord[2])) // Lazy Evaluation
                {
                    intersections.push_back(boxes_mid[ipt].first);
                    ++ipt;
                }
                if(intersections.size() != 0)
                    edges_map[{{i, j, k}, PZ}] = EdgeData(intersections);
            }
            // Checks we haven't forgotten anything
            if(ipt != boxes_mid.size())
                std::cout << "WARNING : An ipt was forgotten.\n";
        }
    for(int i = 0; i < nb_vertices[0]; ++i)
        for(int k = 0; k < nb_vertices[2]; ++k)
        {
            solver.solve(make_interval_from_segment(vertices_grid[i][0][k].coord, vertices_grid[i][nb_vertices[1] - 1][k].coord, t_range));
            const ibex::CovSolverData& cov = solver.get_data();
            final_cov.combine_cov(cov);

            std::vector<std::pair<ibex::IntervalVector, ibex::Vector>> boxes_mid;
            boxes_mid.reserve(cov.size());
            for(size_t ibox = 0; ibox < cov.size(); ++ibox)
                boxes_mid.push_back({cov[ibox], cov[ibox].mid()});

            // On les trie pour simplifier le processus d'assignation aux arêtes.
            auto key = [](const std::pair<ibex::IntervalVector, ibex::Vector>& a, const std::pair<ibex::IntervalVector, ibex::Vector>& b) {return a.second[1] < b.second[1];};
            std::sort(boxes_mid.begin(), boxes_mid.end(), key);
            
            size_t ipt = 0;
            for(int j = 0; j < nb_vertices[1] - 1; ++j)
            {
                std::vector<ibex::IntervalVector> intersections {};
                while((ipt < boxes_mid.size()) && (vertices_grid[i][j][k].coord[1] <= boxes_mid[ipt].second[1]) && (boxes_mid[ipt].second[1] < vertices_grid[i][j+1][k].coord[1]))
                {
                    intersections.push_back(boxes_mid[ipt].first);
                    ++ipt;
                }
                if(intersections.size() != 0)
                    edges_map[{{i, j, k}, PY}] = EdgeData(intersections);
            }
            if(ipt != boxes_mid.size())
                std::cout << "WARNING : An ipt was forgotten.\n";
        }
    for(int j = 0; j < nb_vertices[1]; ++j)
        for(int k = 0; k < nb_vertices[2]; ++k)
        {
            solver.solve(make_interval_from_segment(vertices_grid[0][j][k].coord, vertices_grid[nb_vertices[0] - 1][j][k].coord, t_range));
            const ibex::CovSolverData& cov = solver.get_data(); 
            final_cov.combine_cov(cov);

            std::vector<std::pair<ibex::IntervalVector, ibex::Vector>> boxes_mid;
            boxes_mid.reserve(cov.size());
            for(size_t ibox = 0; ibox < cov.size(); ++ibox)
                boxes_mid.push_back({cov[ibox], cov[ibox].mid()});

            // On les trie pour simplifier le processus d'assignation aux arêtes.
            auto key = [](const std::pair<ibex::IntervalVector, ibex::Vector>& a, const std::pair<ibex::IntervalVector, ibex::Vector>& b) {return a.second[0] < b.second[0];};
            std::sort(boxes_mid.begin(), boxes_mid.end(), key);
            
            size_t ipt = 0;
            for(int i = 0; i < nb_vertices[0] - 1; ++i)
            {
                std::vector<ibex::IntervalVector> intersections {};
                while((ipt < boxes_mid.size()) && (vertices_grid[i][j][k].coord[0] <= boxes_mid[ipt].second[0]) && (boxes_mid[ipt].second[0] < vertices_grid[i+1][j][k].coord[0]))
                {
                    intersections.push_back(boxes_mid[ipt].first);
                    ++ipt;
                }
                if(intersections.size() != 0)
                    edges_map[{{i, j, k}, PX}] = EdgeData(intersections);
            }
            if(ipt != boxes_mid.size())
                std::cout << "WARNING : An ipt was forgotten.\n";
        }
    /*std::vector<Point_3> t_points {};
    std::vector<Vector_3> t_normals {};
    for(auto [_, edge_data] : edges_map)
        for(size_t i = 0; i < edge_data.intersections.size(); ++i)
        {
            t_points.push_back(edge_data.intersections[i]);
            t_normals.push_back(edge_data.normals[i]);
        }*/
    //python_ply_normals("../data/py_maple2.ply", t_points, t_normals);
    std::cout << "Done !\n";
    std::cout << edges_map.size() << " keys in edges_map.\n";
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    print_ibex_cov(*final_cov.pcov);
    if(output_cov_path.size() > 0)
        final_cov.save(output_cov_path.c_str());

    //+A : Creating the connected components (cc) based on the topology of the 4D surface.
    std::cout << "\nContouring the surface...\n";
    start = std::chrono::high_resolution_clock::now();
    ibex::Vector res_eps {0, 0, 0, 0};
    double max_res_eps = 0.;
    for(int i = 0; i < 3; ++i)
    {
        res_eps[i] = (corner_sup[i] - corner_inf[i]) / resolution[i];
        if(max_res_eps < res_eps[i])
            max_res_eps = res_eps[i];
    }
    res_eps[3] = max_res_eps;
    std::cout << "eps_suivi = " << 2*max_res_eps << "\n";
    ibex::DefaultSolver solver2(system, res_eps, 2*max_res_eps);
    //La taille de chaque voxel détermine un critère qui permet de la recouvrir. (c'est le même que celui pour le ball system, faire dessins avec cross caps ?)
    solver2.solve(system.box);
    const ibex::CovSolverData& cov2 = solver2.get_data();
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";
    print_ibex_cov(cov2);

    //Spreads the boxes amongst the voxels
    std::cout << "\nDistributing contour information to cells...\n";
    start = std::chrono::high_resolution_clock::now();
    Matrix_3<std::vector<int>> boxes_per_voxel = initialize(resolution, std::vector<int> {}); 
    for(int ibox = 0; ibox < static_cast<int>(cov2.size()); ++ibox)
    {
        std::array<Index_3, 2> grid_bounds = box_to_grid_index(cov2[ibox], corner_inf, corner_sup, resolution);
        for(int i = grid_bounds[0][0]; i < grid_bounds[1][0]; ++i)
            for(int j = grid_bounds[0][1]; j < grid_bounds[1][1]; ++j)
                for(int k = grid_bounds[0][2]; k < grid_bounds[1][2]; ++k)
                    boxes_per_voxel[i][j][k].push_back(ibox);
    }
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";



    //+B : Compute the singularities.
    std::cout << "\nBoxing ball system (r >> 0 but not yet r = 0)...\n";
    start = std::chrono::high_resolution_clock::now();
    ibex::Function f = system.f_ctrs[0];
    ibex::Function g = system.f_ctrs[1];
    ibex::Variable x("x"), y("y"), z("z"), c("c"), r("r"), t("t");
    ibex::Function S_f(x, y, z, c, r, (f(x, y, z, c + sqrt(r)) + f(x, y, z, c - sqrt(r)))/2, "Ball system Sf");
    ibex::Function S_g(x, y, z, c, r, (g(x, y, z, c + sqrt(r)) + g(x, y, z, c - sqrt(r)))/2, "Ball system Sg");
    ibex::Function D_f(x, y, z, c, r, (f(x, y, z, c + sqrt(r)) - f(x, y, z, c - sqrt(r)))/(2 * sqrt(r)), "Ball system Df (r >> 0)");
    ibex::Function D_g(x, y, z, c, r, (g(x, y, z, c + sqrt(r)) - g(x, y, z, c - sqrt(r)))/(2 * sqrt(r)), "Ball system Dg (r >> 0)");
    ibex::Function dfdt = f.diff()[3]; // AU besoin on pourrait encore plus pousser le DL.
    ibex::Function dgdt = g.diff()[3];
    //ibex::Function f_difft3 = f_difft.diff()[3].diff()[3];
    //ibex::Function g_difft3 = g_difft.diff()[3].diff()[3];
    //ibex::Function df_small(x, y, z, c, r, f_difft(x, y, z, c) + r * f_difft3(x, y, z, c) / 6, "Ball system Df (r ~= 0)");
    //ibex::Function dg_small(x, y, z, c, r, g_difft(x, y, z, c) + r * g_difft3(x, y, z, c) / 6, "Ball system Dg (r ~= 0)");
    //std::cout << f << "\n" << g << "\n" << sf << "\n" << sg << "\n" << df << "\n" << dg << "\n" << df_small << "\n" << dg_small << "\n";
    ibex::SystemFactory factory_double_triple_point;
    ibex::SystemFactory factory_cross_cap;
    factory_double_triple_point.set_simplification_level(3);
    factory_cross_cap.set_simplification_level(3);
    factory_double_triple_point.add_var(x);
    factory_double_triple_point.add_var(y);
    factory_double_triple_point.add_var(z);
    factory_double_triple_point.add_var(c);
    factory_double_triple_point.add_var(r);
    factory_double_triple_point.add_ctr(S_f(x, y, z, c, r) = 0);
    factory_double_triple_point.add_ctr(S_g(x, y, z, c, r) = 0);
    factory_double_triple_point.add_ctr(D_f(x, y, z, c, r) = 0);
    factory_double_triple_point.add_ctr(D_g(x, y, z, c, r) = 0);
    factory_cross_cap.add_var(x);
    factory_cross_cap.add_var(y);
    factory_cross_cap.add_var(z);
    factory_cross_cap.add_var(t);
    factory_cross_cap.add_ctr(f(x, y, z, t) = 0);
    factory_cross_cap.add_ctr(g(x, y, z, t) = 0);
    factory_cross_cap.add_ctr(dfdt(x, y, z, t) = 0);
    factory_cross_cap.add_ctr(dgdt(x, y, z, t) = 0);

    ibex::Function bs(x, y, z, c, r, ibex::Return(S_f(x, y, z, c, r), S_g(x, y, z, c, r), D_f(x, y, z, c, r), D_g(x, y, z, c, r)), "Ball system (whole)");

    ibex::System system_double_triple_point(factory_double_triple_point);
    ibex::Interval r_range {ball_system_threshold, pow(system.box[3].ub() - system.box[3].lb(), 2) / 4};
    ibex::Interval c_range {t_range};
    system_double_triple_point.box = ibex::IntervalVector {system.box[0], system.box[1], system.box[2], c_range, r_range};
    //TODO CHANGER LES VALEURS DE PRECISION
    ibex::Vector res_eps2 {res_eps[0], res_eps[1], res_eps[2], res_eps[3], res_eps[3]};
    std::cout << "epsilon bs = " << 0.5*max_res_eps << "\n";
    ibex::DefaultSolver solver_double_triple_point(system_double_triple_point, 0.3*res_eps2, 0.5*max_res_eps);
    //solver.time_limit = 30;
    //TODO : REUTILISER LE SURFACE CONTOURING,PLUS RAPIDE ?
    solver_double_triple_point.solve(system_double_triple_point.box);
    const ibex::CovSolverData& cov_double_triple_point = solver_double_triple_point.get_data();
    cov_double_triple_point.save("../data/cov/debug1.cov");
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";
    print_ibex_cov(cov_double_triple_point);
    cov_double_triple_point.save("../data/cov/maple_ball_system.cov");

    /*std::cout << "xouxou\n";
    ibex::System system_cross_cap(factory_cross_cap);
    system_cross_cap.box = ibex::IntervalVector {system.box[0], system.box[1], system.box[2], system.box[3];
    std::cout << "\n" << system_cross_cap << "\n";
    ibex::DefaultSolver solver_cross_cap(system_cross_cap, ibex_eps_min, ibex_eps_max);
    solver_cross_cap.solve(system_cross_cap.box);
    const ibex::CovSolverData& cov_cross_cap = solver_cross_cap.get_data();
    donothing(cov_cross_cap);*/
    //cov.save("../../data/cov/maple_ball_system_f2.cov");
    

    //+C : Merge the connected intersections based on the singularities.
    std::cout << "\nDistributing singular points information to cells...\n";
    start = std::chrono::high_resolution_clock::now();
    Matrix_3<std::vector<int>> boxes_per_voxel2 = initialize(resolution, std::vector<int> {});
    for(size_t ibox = 0; ibox < cov_double_triple_point.size(); ++ibox)
    {
        std::array<Index_3, 2> grid_bounds = box_to_grid_index(cov_double_triple_point[ibox], corner_inf, corner_sup, resolution);
        //std::cout << cov_double_triple_point[ibox] << "\n";
        //std::cout << grid_bounds[0] << " " << grid_bounds[1] << "\n";
        //Si la boite est à cheval entre plusieurs cellules, on doit s'assurer que chaque cellule a bien en cet endroit un point double/triple.
        for(int i = grid_bounds[0][0]; i < grid_bounds[1][0]; ++i)
            for(int j = grid_bounds[0][1]; j < grid_bounds[1][1]; ++j)
                for(int k = grid_bounds[0][2]; k < grid_bounds[1][2]; ++k)
                {
                auto& ref1 = vertices_grid[i][j][k].coord;
                auto& ref2 = vertices_grid[i+1][j+1][k+1].coord;
                ibex::IntervalVector intersection = intersect_boxes(cov_double_triple_point[ibox],
                 ibex::IntervalVector {{ref1[0], ref2[0]}, {ref1[1], ref2[1]}, {ref1[2], ref2[2]}, c_range, r_range});
                //std::cout << intersection << "\n";
                if(bs.eval_vector(intersection).contains({0, 0, 0, 0}))
                {
                    boxes_per_voxel2[i][j][k].push_back(ibox);
                    //std::cout << i << " " << j << " " << k << "\n";
                }
                }
    }
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";

    /*for(int i = 0; i < resolution[0]; ++i)
        for(int j = 0; j < resolution[1]; ++j)
            for(int k = 0; k < resolution[2]; ++k)
                std::cout << i << " " << j << " " << k << " " << boxes_per_voxel2[i][j][k] << "\n";
*/

    //For each cell, finds the connected components to group edges intersections together.
    std::cout << "\nGrouping grid / surface intersections by connexity...\n";
    start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < nb_vertices[0]-1; ++i)
        for(int j = 0; j < nb_vertices[1]-1; ++j)
            for(int k = 0; k < nb_vertices[2]-1; ++k)
            {
                std::vector<ibex::IntervalVector*> pintersections {};
                std::vector<ibex::IntervalVector> boxes = {};
                for(int& icov : boxes_per_voxel[i][j][k])
                    boxes.push_back(cov2[icov]);
                for(int& icov : boxes_per_voxel2[i][j][k])
                {
                    ibex::IntervalVector box = cov_double_triple_point[icov];
                    boxes.push_back({box[0], box[1], box[2], t_range});
                }
                int idx_offset = boxes.size();
                for(auto& edge : get_adjacent_edges({i, j, k}))
                    for(auto& inter : edges_map[edge].intersections)
                    {
                        pintersections.push_back(&inter);
                        boxes.push_back(inter);
                    }
                if(pintersections.size() == 0)
                    continue;

                auto [cc, cc_nb] = boxes_connected_components(boxes);
                voxels_map[{i, j, k}].connected_intersections = std::vector<std::vector<ibex::IntervalVector*>>(cc_nb, std::vector<ibex::IntervalVector*> {});

                for(int idx = 0; idx < static_cast<int>(pintersections.size()); ++idx)
                    voxels_map[{i, j, k}].connected_intersections[cc[idx + idx_offset]].push_back(pintersections[idx]);

                //Some of the boxes can be here "leaking" from another cell, and be in their own cc which doesn't intersect truly with the grid. Those must be erased.
                auto& ref = voxels_map[{i, j, k}].connected_intersections;
                for(int idx = ref.size() - 1; idx >= 0; --idx)
                    if(ref[idx].empty())
                        ref.erase(ref.begin() + idx);
                
                voxels_map[{i, j, k}].dual_points = std::vector<Point_3>(ref.size(), Point_3 {});
            }
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";


    // 4. TODO ! Résoudre QED.
    // Fusionner avec étape précédante ?
    std::cout << "\nSolving QEDs...\n";
    start = std::chrono::high_resolution_clock::now();
    for(auto& [idx, data] : voxels_map)
    {
        for(int cc_idx = 0; cc_idx < static_cast<int>(data.connected_intersections.size()); ++cc_idx)
        {
            if(idx == Index_3 {2, 5, 4})
                donothing(0);
            std::vector<Point_3> adj_intersections {};
            std::vector<Vector_3> adj_normals {};
            for(auto pinter : data.connected_intersections[cc_idx])
            {
                ibex::Vector mid_pt = pinter->mid();
                Point_3 pt = {mid_pt[0], mid_pt[1], mid_pt[2]};
                adj_intersections.push_back(pt);
                adj_normals.push_back(get_normal(dfg, pt));
            }
            Point_3 sol = solve_QED(adj_intersections, adj_normals, vertices_grid[idx[0]][idx[1]][idx[2]].coord, vertices_grid[idx[0]+1][idx[1]+1][idx[2]+1].coord);
            data.dual_points[cc_idx] = sol;
        }
    }
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";

    // 5. Création quadrangulation.
    //Attributes to each dual point an id
    std::cout << "\nWriting the mesh...\n";
    start = std::chrono::high_resolution_clock::now();
    std::unordered_map<Index_4, int, Index_4Hasher> vertex_to_id {};
    int iid = 0;
    for(const auto& [idx, data] : voxels_map)
        for(int ipt = 0; ipt < static_cast<int>(data.dual_points.size()); ++ipt)
        {
            vertex_to_id[{idx[0], idx[1], idx[2], ipt}] = iid;
            ++iid;
        }

    Mesh mesh {}; // Les arêtes pouvant être sur le bord de la boite, certaines arêtes peuvent n'être entourées que de 1 ou 2 arêtes. Pour l'instant on les note tel quel. A l'avenir, enlever les bords de la boite pour éviter ce problème ?
    for(auto& [edge, edge_data] : edges_map)
    {
        std::vector<Index_3> adj_voxels = get_adjacent_voxels(edge, resolution);
        std::vector<Face> faces_built {};        
        for(auto& pt : edge_data.intersections)
        {
            Face face {};
            for(auto& ivoxel : adj_voxels)
            {
                //On cherche à quel point dual dans le voxel cette arête correspond.
                int ipt = -1;
                for(int icc = 0; icc < static_cast<int>(voxels_map[ivoxel].connected_intersections.size()); ++icc)
                    for(auto& ppt : voxels_map[ivoxel].connected_intersections[icc])
                        if(ppt == &pt)
                        {
                            ipt = icc;
                        }
                assert(ipt != -1);
                face.push_back(vertex_to_id[{ivoxel[0], ivoxel[1], ivoxel[2], ipt}]);
            }
            if(std::find(faces_built.begin(), faces_built.end(), face) == faces_built.end())
                faces_built.push_back(face);
        }
        for(Face face : faces_built)
            mesh.push_back(face);
    }

    std::vector<Point_3> points(vertex_to_id.size());
    for(auto& [ivoxel, id] : vertex_to_id)
        points[id] = voxels_map[{ivoxel[0], ivoxel[1], ivoxel[2]}].dual_points[ivoxel[3]];

    // Rajout de la grille dans le maillage pour débugger
    if(grid_in_mesh)
        add_grid_to_mesh(mesh, points, vertices_grid, nb_vertices);

    write_to_ply(output_mbx_path, mesh, points);
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Time taken : " << duration.count() << "ms.\n";
    std::cout << "Done !\n";

    return 0;
}